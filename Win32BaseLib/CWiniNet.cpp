#include "CWinInet.h"


inline void MyParseUrlW(LPCWSTR lpUrl, std::wstring& strHostName, std::wstring& strPage, WORD& sPort)
{
	sPort = 80;
	std::wstring strTemp(lpUrl);
	int nPos = strTemp.find(L"http://");
	if (std::wstring::npos != nPos)
		strTemp = strTemp.substr(nPos + 7, strTemp.size() - nPos - 7);
	else
	{
		nPos = strTemp.find(L"https://");
		if (std::wstring::npos != nPos)
		{
			sPort = 443;//INTERNET_DEFAULT_HTTPS_PORT;
			strTemp = strTemp.substr(nPos + 8, strTemp.size() - nPos - 8);
		}
	}
	nPos = strTemp.find('/');
	if (std::wstring::npos == nPos)//没有找到 /
		strHostName = strTemp;
	else
		strHostName = strTemp.substr(0, nPos);
	int nPos1 = strHostName.find(':');
	if (nPos1 != std::wstring::npos)
	{
		std::wstring strPort = strTemp.substr(nPos1 + 1, strHostName.size() - nPos1 - 1);
		strHostName = strHostName.substr(0, nPos1);
		sPort = (WORD)_wtoi(strPort.c_str());
	}
	if (std::wstring::npos == nPos)
		return;
	strPage = strTemp.substr(nPos, strTemp.size() - nPos);
}

inline void MyParseUrlA(LPCSTR lpUrl, std::string& strHostName, std::string& strPage, WORD& sPort)
{
	sPort = 80;
	std::string strTemp(lpUrl);
	int nPos = strTemp.find("http://");
	if (std::string::npos != nPos)
		strTemp = strTemp.substr(nPos + 7, strTemp.size() - nPos - 7);
	else
	{
		nPos = strTemp.find("https://");
		if (std::wstring::npos != nPos)
		{
			sPort = 443;//INTERNET_DEFAULT_HTTPS_PORT;
			strTemp = strTemp.substr(nPos + 8, strTemp.size() - nPos - 8);
		}
	}
	nPos = strTemp.find('/');
	if (std::string::npos == nPos)//没有找到 /
		strHostName = strTemp;
	else
		strHostName = strTemp.substr(0, nPos);
	int nPos1 = strHostName.find(':');
	if (nPos1 != std::string::npos)
	{
		std::string strPort = strTemp.substr(nPos1 + 1, strHostName.size() - nPos1 - 1);
		strHostName = strHostName.substr(0, nPos1);
		sPort = (WORD)atoi(strPort.c_str());
	}
	if (std::string::npos == nPos)
		return;
	strPage = strTemp.substr(nPos, strTemp.size() - nPos);
}

inline std::wstring Utf2U(const std::string& strUtf8)
{
	std::wstring wstrRet(L"");
	int nLen = MultiByteToWideChar(CP_UTF8, 0, strUtf8.c_str(), -1, NULL, 0);
	if (nLen == ERROR_NO_UNICODE_TRANSLATION)
		throw "Utf8ToUnicode出错：无效的UTF-8字符串。";
	wstrRet.resize(nLen + 1, '\0');
	MultiByteToWideChar(CP_UTF8, 0, strUtf8.c_str(), -1, (LPWSTR)wstrRet.c_str(), nLen);
	return wstrRet;
}

inline std::string U2A(const std::wstring& str)
{
	std::string strDes;
	if (str.empty())
		goto __end;
	int nLen = ::WideCharToMultiByte(CP_ACP, 0, str.c_str(), str.size(), NULL, 0, NULL, NULL);
	if (0 == nLen)
		goto __end;
	char* pBuffer = new char[nLen + 1];
	memset(pBuffer, 0, nLen + 1);
	::WideCharToMultiByte(CP_ACP, 0, str.c_str(), str.size(), pBuffer, nLen, NULL, NULL);
	pBuffer[nLen] = '\0';
	strDes.append(pBuffer);
	delete[] pBuffer;
__end:
	return strDes;
}

inline std::wstring A2U(const std::string& str)
{
	std::wstring strDes;
	if (str.empty())
		goto __end;
	int nLen = ::MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), NULL, 0);
	if (0 == nLen)
		goto __end;
	wchar_t* pBuffer = new wchar_t[nLen + 1];
	memset(pBuffer, 0, nLen + 1);
	::MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), pBuffer, nLen);
	pBuffer[nLen] = '\0';
	strDes.append(pBuffer);
	delete[] pBuffer;
__end:
	return strDes;
}

inline bool FileExistA(LPCSTR lpFile)
{
	HANDLE hFile = CreateFileA(lpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return false;
	CloseHandle(hFile);
	return true;
}

inline bool FileExistW(LPCWSTR lpFile)
{
	HANDLE hFile = CreateFileW(lpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return false;
	CloseHandle(hFile);
	return true;
}



CWininetHttp::CWininetHttp(void)
	: m_hSession(NULL)
	, m_hConnect(NULL)
	, m_hRequest(NULL)
	, m_bHttps(false)
{
	memset(&m_paramsData, 0, sizeof(HttpParamsData));
}

CWininetHttp::~CWininetHttp(void)
{
	Release();
}

string CWininetHttp::Request(LPCSTR lpUrl, HttpRequest type, LPCSTR lpPostData/* = NULL*/, LPCSTR lpHeader/*=NULL*/)
{
	string strRet;
	try
	{
		if (lpUrl == NULL || strlen(lpUrl) == 0)
			throw HttpErrorParam;
		Release();
		m_hSession = InternetOpen(L"Http-connect", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, NULL);
		if (NULL == m_hSession)
			throw HttpErrorInit;
		INTERNET_PORT port = INTERNET_DEFAULT_HTTP_PORT;
		string strHostName, strPageName;
		MyParseUrlA(lpUrl, strHostName, strPageName, port);
		if (port == INTERNET_DEFAULT_HTTPS_PORT)
			m_bHttps = true;
		m_hConnect = InternetConnectA(m_hSession, strHostName.c_str(), port, NULL, NULL, INTERNET_SERVICE_HTTP, NULL, NULL);
		if (NULL == m_hConnect)
			throw HttpErrorConnect;
		DWORD dwFlags = INTERNET_FLAG_RELOAD;
		if (m_bHttps)
			dwFlags |= (INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID);
		m_hRequest = HttpOpenRequestA(m_hConnect, (type == HttpGet) ? "GET" : "POST", strPageName.c_str(), "HTTP/1.1", NULL, NULL, dwFlags, NULL);
		if (NULL == m_hRequest)
			throw HttpErrorInit;
		BOOL bRet = FALSE;
		DWORD dwHeaderSize = (NULL == lpHeader) ? 0 : strlen(lpHeader);
		DWORD dwSize = (lpPostData == NULL) ? 0 : strlen(lpPostData);
		bRet = HttpSendRequestA(m_hRequest, lpHeader, dwHeaderSize, (LPVOID)lpPostData, dwSize);
		if (!bRet)
			throw HttpErrorSend;
		char szBuffer[READ_BUFFER_SIZE + 1] = { 0 };
		DWORD dwReadSize = READ_BUFFER_SIZE;
		if (!HttpQueryInfoA(m_hRequest, HTTP_QUERY_RAW_HEADERS, szBuffer, &dwReadSize, NULL))
			throw HttpErrorQuery;
		if (NULL != strstr(szBuffer, "404"))
			throw HttpError404;
		while (true)
		{
			bRet = InternetReadFile(m_hRequest, szBuffer, READ_BUFFER_SIZE, &dwReadSize);
			if (!bRet || (0 == dwReadSize))
				break;
			szBuffer[dwReadSize] = '\0';
			strRet.append(szBuffer, dwReadSize);
		}
	}
	catch (HttpInterfaceError error)
	{
		m_paramsData.errcode = error;
	}
	return strRet;
}

string CWininetHttp::Request(LPCWSTR lpUrl, HttpRequest type, LPCSTR lpPostData/* = NULL*/, LPCWSTR lpHeader/*=NULL*/)
{
	string strRet;
	try
	{
		if (NULL == lpUrl || wcslen(lpUrl) == 0)
			throw HttpErrorParam;
		Release();
		m_hSession = InternetOpen(L"Http-connect", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, NULL);
		if (NULL == m_hSession)
			throw HttpErrorInit;
		INTERNET_PORT port = INTERNET_DEFAULT_HTTP_PORT;
		wstring strHostName, strPageName;
		MyParseUrlW(lpUrl, strHostName, strPageName, port);
		if (port == INTERNET_DEFAULT_HTTPS_PORT)
			m_bHttps = true;
		m_hConnect = InternetConnectW(m_hSession, strHostName.c_str(), port, NULL, NULL, INTERNET_SERVICE_HTTP, NULL, NULL);
		if (NULL == m_hConnect)
			throw HttpErrorConnect;
		DWORD dwFlags = INTERNET_FLAG_RELOAD;
		if (m_bHttps)
			dwFlags |= (INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID);
		m_hRequest = HttpOpenRequestW(m_hConnect, (type == HttpGet) ? L"GET" : L"POST", strPageName.c_str(), L"HTTP/1.1", NULL, NULL, dwFlags, NULL);
		if (NULL == m_hRequest)
			throw HttpErrorInit;
		BOOL bRet = FALSE;
		DWORD dwHeaderSize = (NULL == lpHeader) ? 0 : wcslen(lpHeader);
		DWORD dwSize = (lpPostData == NULL) ? 0 : strlen(lpPostData);
		bRet = HttpSendRequestW(m_hRequest, lpHeader, dwHeaderSize, (LPVOID)lpPostData, dwSize);
		if (!bRet)
			throw HttpErrorSend;
		char szBuffer[READ_BUFFER_SIZE + 1] = { 0 };
		DWORD dwReadSize = READ_BUFFER_SIZE;
		if (!HttpQueryInfoA(m_hRequest, HTTP_QUERY_RAW_HEADERS, szBuffer, &dwReadSize, NULL))
			throw HttpErrorQuery;
		if (NULL != strstr(szBuffer, "404"))
			throw HttpError404;
		while (true)
		{
			bRet = InternetReadFile(m_hRequest, szBuffer, READ_BUFFER_SIZE, &dwReadSize);
			if (!bRet || (0 == dwReadSize))
				break;
			szBuffer[dwReadSize] = '\0';
			strRet.append(szBuffer, dwReadSize);
		}
	}
	catch (HttpInterfaceError error)
	{
		m_paramsData.errcode = error;
	}
	return strRet;
}

bool CWininetHttp::DownloadFile(LPCSTR lpUrl, LPCSTR lpFilePath)
{
	bool bResult = false;
	BYTE* pBuffer = NULL;
	FILE* fp = NULL;

	wstring strUrl = A2U(string(lpUrl));
	wstring strFilePath = A2U(string(lpFilePath));

	try
	{
		if (NULL == strUrl.c_str() || wcslen(strUrl.c_str()) == 0)
			throw HttpErrorIllegalUrl;
		Release();
		m_hSession = InternetOpen(L"Http-connect", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, NULL);
		if (NULL == m_hSession) throw HttpErrorInit;
		INTERNET_PORT port = INTERNET_DEFAULT_HTTP_PORT;
		wstring strHostName, strPageName;
		MyParseUrlW(strUrl.c_str(), strHostName, strPageName, port);
		if (port == INTERNET_DEFAULT_HTTPS_PORT)
			m_bHttps = true;
		m_hConnect = InternetConnectW(m_hSession, strHostName.c_str(), port, NULL, NULL, INTERNET_SERVICE_HTTP, NULL, NULL);
		if (NULL == m_hConnect) throw HttpErrorConnect;
		//INTERNET_FLAG_IGNORE_CERT_CN_INVALID：忽略因服务器的证书主机名与请求的主机名不匹配所导致的错误。
		//INTERNET_FLAG_IGNORE_CERT_DATE_INVALID：忽略由已失效的服务器证书导致的错误。
		DWORD dwFlags = INTERNET_FLAG_RELOAD;
		if (m_bHttps)
			dwFlags |= (INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID);
		m_hRequest = HttpOpenRequestW(m_hConnect, L"GET", strPageName.c_str(), L"HTTP/1.1", NULL, NULL, dwFlags, NULL);
		if (NULL == m_hRequest) throw HttpErrorInit;
		BOOL bRet = HttpSendRequestW(m_hRequest, NULL, 0, NULL, 0);
		if (!bRet) throw HttpErrorSend;
		char szBuffer[1024 + 1] = { 0 };
		DWORD dwReadSize = 1024;
		bRet = HttpQueryInfoA(m_hRequest, HTTP_QUERY_RAW_HEADERS, szBuffer, &dwReadSize, NULL);
		if (!bRet) throw HttpErrorQuery;
		string strRetHeader = szBuffer;
		if (string::npos != strRetHeader.find("404")) throw HttpError404;
		dwReadSize = 1024;
		bRet = HttpQueryInfoA(m_hRequest, HTTP_QUERY_CONTENT_LENGTH, szBuffer, &dwReadSize, NULL);
		if (!bRet) throw HttpErrorQuery;
		szBuffer[dwReadSize] = '\0';
		const double uFileSize = atof(szBuffer);
		int nMallocSize = uFileSize<DOWNLOAD_BUFFER_SIZE ? (int)uFileSize : DOWNLOAD_BUFFER_SIZE;
		pBuffer = (BYTE*)malloc(nMallocSize);
		int nFindPos = 0;
		wstring strSavePath(strFilePath);
		while (wstring::npos != (nFindPos = strSavePath.find(L"\\", nFindPos)))
		{
			wstring strChildPath = strSavePath.substr(0, nFindPos);
			if (INVALID_FILE_ATTRIBUTES == ::GetFileAttributes(strChildPath.c_str()))
				CreateDirectory(strChildPath.c_str(), NULL);
			nFindPos++;
		}
		_wfopen_s(&fp, strSavePath.c_str(), L"wb+");
		if (NULL == fp) throw HttpErrorCreateFile;
		double uWriteSize = 0;
		while (true)
		{
			bRet = InternetReadFile(m_hRequest, pBuffer, nMallocSize, &dwReadSize);
			if (!bRet || (0 == dwReadSize))
				break;
			size_t nWrite = fwrite(pBuffer, dwReadSize, 1, fp);
			if (nWrite == 0)
				throw HttpErrorWriteFile;
			uWriteSize += dwReadSize;
			if (m_paramsData.callback)
				m_paramsData.callback->OnDownloadCallback(m_paramsData.lpparam, DS_Loading, uFileSize, uWriteSize);
		}
		fclose(fp);
		if (uFileSize != uWriteSize) throw HttpErrorDownload;
		if (m_paramsData.callback)
			m_paramsData.callback->OnDownloadCallback(m_paramsData.lpparam, DS_Finished, uFileSize, uWriteSize);
		bResult = true;
	}
	catch (HttpInterfaceError error)
	{
		m_paramsData.errcode = error;
		if (m_paramsData.callback)
			m_paramsData.callback->OnDownloadCallback(m_paramsData.lpparam, DS_Fialed, 0, 0);
	}
	if (pBuffer)
		free(pBuffer);
	if (fp)
		fclose(fp);
	return bResult;
}

bool CWininetHttp::DownloadToMem(LPCWSTR lpUrl, OUT void** ppBuffer, OUT int* nSize)
{
	bool bResult = false;
	BYTE *pDesBuffer = NULL;
	try
	{
		if (NULL == lpUrl || wcslen(lpUrl) == 0)
			throw HttpErrorIllegalUrl;
		Release();
		m_hSession = InternetOpen(L"Http-connect", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, NULL);
		if (NULL == m_hSession) throw HttpErrorInit;
		INTERNET_PORT port = INTERNET_DEFAULT_HTTP_PORT;
		wstring strHostName, strPageName;
		MyParseUrlW(lpUrl, strHostName, strPageName, port);
		if (port == INTERNET_DEFAULT_HTTPS_PORT)
			m_bHttps = true;
		m_hConnect = InternetConnectW(m_hSession, strHostName.c_str(), port, NULL, NULL, INTERNET_SERVICE_HTTP, NULL, NULL);
		if (NULL == m_hConnect) throw HttpErrorConnect;
		DWORD dwFlags = INTERNET_FLAG_RELOAD;
		if (m_bHttps)
			dwFlags |= (INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID);
		m_hRequest = HttpOpenRequestW(m_hConnect, L"GET", strPageName.c_str(), L"HTTP/1.1", NULL, NULL, dwFlags, NULL);
		if (NULL == m_hRequest) throw HttpErrorInit;
		BOOL bRet = HttpSendRequestW(m_hRequest, NULL, 0, NULL, 0);
		if (!bRet) throw HttpErrorSend;
		wchar_t szBuffer[1024 + 1] = { 0 };
		DWORD dwReadSize = 1024;
		bRet = HttpQueryInfoW(m_hRequest, HTTP_QUERY_RAW_HEADERS, szBuffer, &dwReadSize, NULL);
		if (!bRet) throw HttpErrorQuery;
		wstring strRetHeader(szBuffer);
		if (string::npos != strRetHeader.find(L"404")) throw HttpError404;
		dwReadSize = 1024;
		bRet = HttpQueryInfoW(m_hRequest, HTTP_QUERY_CONTENT_LENGTH, szBuffer, &dwReadSize, NULL);
		bool bHasLength = GetLastError() != ERROR_HTTP_HEADER_NOT_FOUND;
		if (!bRet && bHasLength)
			throw HttpErrorQuery;
		int uFileSize = 0;
		if (bHasLength)
		{
			szBuffer[dwReadSize] = '\0';
			uFileSize = _wtoi(szBuffer);
			if (uFileSize > DOWNLOAD_BUFFER_SIZE || uFileSize < 0)
				throw HttpErrorBuffer;//文件大小超过预设最大值，不建议下载到内存
		}
		else
		{
			uFileSize = DOWNLOAD_BUFFER_SIZE;
		}
		pDesBuffer = (BYTE*)malloc(uFileSize);
		int uWriteSize = 0;
		while (true)
		{
			bRet = InternetReadFile(m_hRequest, pDesBuffer + uWriteSize, uFileSize, &dwReadSize);
			if (!bRet || (0 == dwReadSize))
				break;
			uWriteSize += dwReadSize;
		}
		if (bHasLength && uFileSize != uWriteSize)
			throw HttpErrorDownload;
		*ppBuffer = pDesBuffer;
		*nSize = uWriteSize;
		bResult = true;
	}
	catch (HttpInterfaceError error)
	{
		m_paramsData.errcode = error;
		if (pDesBuffer)
		{
			free(pDesBuffer);
			pDesBuffer = NULL;
		}
	}
	return bResult;
}

void CWininetHttp::SetDownloadCallback(IHttpCallback* pCallback, void* pParam)
{
	m_paramsData.callback = pCallback;
	m_paramsData.lpparam = pParam;
}



void CWininetHttp::ReleaseHandle(HINTERNET & hInternet)
{
	if (hInternet)
	{
		InternetCloseHandle(hInternet);
		hInternet = NULL;
	}
}

void CWininetHttp::Release()
{
	ReleaseHandle(m_hRequest);
	ReleaseHandle(m_hConnect);
	ReleaseHandle(m_hSession);
}