#pragma once
/*************************************************
*@file  CSystemInfo.h
*@brief 系统信息的获取相关操作
**************************************************/
#include <Windows.h>
#include <string>
/*************************************************
*@class CSystemInfo CSystemInfo.h
*@brief 系统信息的获取相关操作
**************************************************/


class CSystemInfo
{
	enum  WINDOWS_VERSIONEX
	{
		WIN_UNKNOWN = 0,
		WIN_2000 = 1,
		WIN_XP,
		WIN_XP_X64,
		WIN_SERVER_2003,
		WIN_HOME_SERVER,
		WIN_SERVER_2003_R2,
		WIN_VISTA,
		WIN_SERVER_2008,
		WIN_SERVER_2008_R2,
		WIN_7,
		WIN_SERVER_2012,
		WIN_8,
		WIN_SERVER_2012_R2,
		WIN_8_1,
		WIN_10,
		WIN_SERVER_2016_TP,
	};

public:
	static				    void CreateLink(const char *iconExePath, const  char *iconExeFile, const char *commline, const char *iconName, const  char *iconImageFile);
	static std::string		GetDefaultBrowserPath();
	static std::string		GetIePath();
	static std::string		GetIeVer();
	static DWORD			GetSysVer();
	static std::string		GetSysVer(WINDOWS_VERSIONEX flg);
	std::string				CreateLinkIcon(const char * iconExePath, const char * iconExeFile, const char * commline , const char * iconName, const char * iconImageFile);
	bool					CreateTaskBarLinkIcon(DWORD sysVer,std::string linkPath, std::string linkexe);
	void					TaskbarPinWin10(LPCSTR lpszFilePath, LPCSTR lpszFileName);
	BOOL					TaskbarPin7(LPSTR lpPath, LPSTR lpFileName, BOOL bIsPin);
private:

};

