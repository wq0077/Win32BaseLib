#pragma once
/*************************************************
*@file  CWininetHttp.h
*@brief 编写与HTTP获取数据有关
**************************************************/
#include <Windows.h>
#include <string>
#include <iostream>
#include <Wininet.h>
#pragma comment(lib, "Wininet.lib")
using std::string;
using std::wstring;
//WININETHTTP
// 每次读取的字节数
#define READ_BUFFER_SIZE		4096
#define DOWNLOAD_BUFFER_SIZE	8*1024*1024	//8M的缓存

enum HttpRequest
{
	HttpGet = 0,
	HttpPost,
};
//枚举下载状态
enum DownloadState
{
	DS_Loading = 0,
	DS_Fialed,
	DS_Finished,
};

/******************************************************
*定义错误信息
*
******************************************************/
enum HttpInterfaceError
{
	HttpErrorSuccess = 0,		//成功
	HttpErrorInit,				//初始化失败
	HttpErrorConnect,			//连接HTTP服务器失败
	HttpErrorSend,				//发送请求失败
	HttpErrorQuery,				//查询HTTP请求头失败
	HttpError404,				//页面不存在
	HttpErrorIllegalUrl,		//无效的URL
	HttpErrorCreateFile,		//创建文件失败
	HttpErrorDownload,			//下载失败
	HttpErrorQueryIP,			//获取域名对应的地址失败
	HttpErrorSocket,			//套接字错误
	HttpErrorUserCancel,		//用户取消下载
	HttpErrorBuffer,			//文件太大，缓冲区不足
	HttpErrorHeader,			//HTTP请求头错误
	HttpErrorParam,				//参数错误，空指针，空字符……
	HttpErrorWriteFile,			//写入文件失败
	HttpErrorUnknow,

};

//下载的回调
class IHttpCallback
{
public:
	virtual void	OnDownloadCallback(void* pParam, DownloadState state, double nTotalSize, double nLoadSize) = 0;
	virtual bool	IsNeedStop() = 0;
};

struct HttpParamsData
{
	void *lpparam;
	IHttpCallback *callback;
	HttpInterfaceError errcode;
};

enum InterfaceType
{
	TypeSocket = 0,
	TypeWinInet,
	TypeWinHttp,
};


class CWininetHttp
{
public:
	CWininetHttp(void);
	virtual ~CWininetHttp(void);
	virtual string	Request(LPCSTR pUrl, HttpRequest type = HttpGet, LPCSTR pPostData = NULL, LPCSTR pHeader = NULL);
	virtual string	Request(LPCWSTR pUrl, HttpRequest type = HttpGet, LPCSTR pPostData = NULL, LPCWSTR pHeader = NULL);
	virtual bool	DownloadFile(LPCSTR lpUrl, LPCSTR lpFilePath);
	virtual bool	DownloadToMem(LPCWSTR lpUrl, OUT void** ppBuffer, OUT int* nSize);
	virtual void	SetDownloadCallback(IHttpCallback* pCallback, void* pParam);
	virtual HttpInterfaceError GetErrorCode() { return m_paramsData.errcode; }
	virtual	void	FreeInstance() { delete this; }

protected:
	//关闭句柄
	void	ReleaseHandle(HINTERNET& hInternet);
	void	Release();

private:
	bool		m_bHttps;
	HINTERNET	m_hSession;
	HINTERNET	m_hConnect;
	HINTERNET	m_hRequest;
	HttpParamsData m_paramsData;
};



