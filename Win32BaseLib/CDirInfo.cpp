#include "stdafx.h"
#include "CDirInfo.h"

namespace Win32BaseLib
{
	/*!
	* 构造函数
	*/
	CDirInfo::CDirInfo()
	{
	}

	/*!
	* 构造函数
	* @param string& dir  任意绝对路径，或相对路径，或目录名  
	*/
	CDirInfo::CDirInfo(std::string& dirPath)
	{
		SaveDirPath(dirPath);
	}

	/*!
	* 构造函数
	* @param string dir  任意绝对路径，或相对路径，或目录名
	*/
	CDirInfo::CDirInfo(std::string dirPath)
	{
		SaveDirPath(dirPath);
	}
	std::string CDirInfo::GetCurrentAppDir()
	{
		return std::string();
	}

	/*!
	* @brief SetPath
	*
	* 获取指定PID进程的完整路径
	* @param  pid     进程PID 
	* @return string  进程EXE完整路径
	*/
	std::string CDirInfo::GetProcessFullPath(DWORD pid)
	{
		HANDLE hpro = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
		if (!hpro)
			return false;
		DWORD len = MAX_PATH;
		BOOL bok = false;
		DWORD dw = 0;
		CHAR* path = NULL;
		do
		{
			path = new CHAR[len]();
			bok = QueryFullProcessImageNameA(hpro, 0, path, &len);
			if (bok)
			{
				break;
			}
			else
			{
				dw = GetLastError();
				if (dw == 122)
				{
					delete path;
					path = NULL;
				}
				else
					break;
			}
			len = len * 2;
		} while (true);
		CloseHandle(hpro);
		return std::string(path);
	}

	/*!
	* @brief GetProcessDir
	*
	* 获取指定PID进程EXE所在目录
	* @param  pid     进程PID
	* @return string  进程EXE所在目录
	*/
	std::string CDirInfo::GetProcessDir(DWORD pid)
	{
		std::string exeDir = GetProcessFullPath(pid);
		return CdUp(exeDir);
	}

	/*!
	* @brief Exists
	*
	* 判断指定目录路径是否存在
	* @param  string   目录路径
	* @return bool  目录是否存在
	*/
	bool CDirInfo::Exists(std::string path)
	{
		return PathIsDirectoryA(path.c_str());
	}

	/*!
	* @brief GetSysPath
	*
	* 返回给定的系统环境变量路径
	* @param  sysName  系统环境变量
	* @return void
	*/
	std::string CDirInfo::GetSysPath(std::string sysName)
	{
		if (sysName[0] == '%' && sysName[sysName.length()-1] == '%')
		{
			#define INFO_BUFFER_SIZE 32767
			CHAR  infoBuf[INFO_BUFFER_SIZE];
			DWORD  bufCharCount = INFO_BUFFER_SIZE;
			bufCharCount = ExpandEnvironmentStringsA(sysName.c_str(), infoBuf,
				INFO_BUFFER_SIZE);
			if (bufCharCount > INFO_BUFFER_SIZE)
				return std::string();
			else if (!bufCharCount)
				return std::string();
			else
				_tprintf(TEXT("\n   %s"), infoBuf);
			return std::string(infoBuf);
		}
		return std::string();
	}

	/*!
	* @brief SetCurrentAppWorkPath
	*
	* 设置当前程序工作目录
	* @param  dirPath  有效目录路径
	* @return bool
	*/
	bool CDirInfo::SetCurrentAppWorkPath(std::string dirPath)
	{
		if (PathIsDirectoryA(dirPath.c_str()))
		{
			return SetCurrentDirectoryA(dirPath.c_str());
		}
		return false;
	}

	/*!
	* @brief GetPathName
	*
	* 获取目录路径中最后名称部份,文件路径也可
	* @param  dirPath  完整路径
	* @return 文件,名称部份
	*/
	std::string CDirInfo::GetPathName(std::string dirPath)
	{
		std::string fullPath = GetCurrentWorkFullPath();
		char *tempPath = new char[fullPath.length() + 1]();
		sprintf_s(tempPath, fullPath.length() + 1, "%s", fullPath.c_str());
		PathRemoveBlanksA(tempPath);
		PathRemoveBackslashA(tempPath);
		PathStripPathA(tempPath);
		return std::string(tempPath);
	}

	/*!
	* @brief SaveDirPath
	*
	* 构造时使用，保存目录的有效性
	* @param  sysName  系统环境变量
	* @return void
	*/
	bool CDirInfo::SaveDirPath(std::string dirPath)
	{
		char *tempPath = new char[dirPath.length() + 1]();
		sprintf_s(tempPath, dirPath.length() + 1, "%s", dirPath.c_str());
		PathRemoveBlanksA(tempPath);
		PathRemoveBackslashA(tempPath);
		if ( !(GetFileAttributesA(tempPath) & FILE_ATTRIBUTE_DIRECTORY))
		{
			PathRemoveFileSpecA(tempPath);
		}	
		m_dirPath = tempPath;
		return PathIsDirectoryA(tempPath);
	}

	
	/*!
	* @brief SetPath
	*
	* 给当前对象设置路径
	* @param  path  任意绝对路径，或相对路径，或目录名  
	* @return void   
	*/
	void CDirInfo::SetPath(std::string dirPath)
	{
		SaveDirPath(dirPath);
	}

	/*!
	* @brief GetPath
	*
	* 返回当前对象设置路径
	* @return string
	*/
	std::string CDirInfo::GetPath()
	{
		return m_dirPath;
	}

	/*!
	* 析构函数
	*/
	CDirInfo::~CDirInfo()
	{
	}

	/*!
	* @brief GetAbsoluteFilePath
	*
	* 获取指定文件名的绝对路径，直接或者搜索后
	* @param  fileName 此目录下的文件名,或相对路径
	* @return string   此文件的绝对路径,不检查是否存在
	*/
	std::string CDirInfo::GetAbsoluteFilePath(std::string path)
	{
		std::string strPath = m_dirPath + "\\" + path;
			//直接返回拼接路径。
		return GetCanonicalizePath(strPath);
	}

	/*!
	* @brief GetRelativeFilePath
	*
	* 获取指定文件名的相对路径
	* @param  string fileName  此目录下的文件名,或相对路径
	* @return string  此文件相对此DIR的相对路径
	*/
	std::string CDirInfo::GetRelativeFilePath(std::string fileName)
	{
		char relativePath[MAX_PATH] = "";		
		if (PathRelativePathToA(relativePath, m_dirPath.c_str(), GetFileAttributesA(m_dirPath.c_str()), fileName.c_str(), GetFileAttributesA(fileName.c_str())))
		{
			return std::string(relativePath);
		}
		return fileName;	
	}

	/*
	* @brief Cd
	*
	* 设置目录为指定的新目录
	* @param string   新的目录名或路径
	* 返回值
	*/
	bool CDirInfo::Cd(std::string newDirPath)
	{
		m_dirPath = newDirPath;
		return true;
	}

	/*!
	* @brief CdUp
	*
	* 设置为父目录路径
	* @return bool 
	*/
	bool CDirInfo::CdUp()
	{
		m_dirPath =	CdUp(m_dirPath);
		return true;
	}

	/*!
	* @brief CdUp
	*
	* 获取指定路径的父目录路径
	* @return string
	*/
	std::string	 CDirInfo::CdUp(std::string dirPath)
	{
		char *tempPath = new char[dirPath.length() + 1]();
		sprintf_s(tempPath, dirPath.length() + 1, "%s", dirPath.c_str());
		PathRemoveBlanksA(tempPath);
		PathRemoveFileSpecA(tempPath);
		return tempPath;
	}

	/*!
	* @brief GetObjectCount
	*
	* 获取当前目录下所有文件或目录的总数
	* @return DWORD  总数量
	*/
	DWORD CDirInfo::GetObjectCount()
	{
		return  CDirInfo::GetObjectCount(m_dirPath);
	}

	/*!
	* @brief GetObjectCount
	*
	* 获取当前目录下所有文件或目录的总数
	* @return DWORD  总数量
	*/
	DWORD CDirInfo::GetObjectCount(const std::string dirPath)
	{
		DWORD dwCount = 0;
		if (!Exists(dirPath))
			return dwCount;
		HANDLE hFirstFile;
		WIN32_FIND_DATAA fileData;
		DWORD dwError = 0;
		std::string tempPath = dirPath + "\\*.*";
		hFirstFile = FindFirstFileA(tempPath.c_str(),&fileData);
		if (INVALID_HANDLE_VALUE == hFirstFile)
		{
			return 0;
		}
		while (true)
		{
			if (!FindNextFileA(hFirstFile, &fileData) != 0)
			{
				dwError = GetLastError();
				if (dwError == ERROR_NO_MORE_FILES)
					break;
				break;
			}
			if (!strcmp(fileData.cFileName, ".") || !strcmp(fileData.cFileName, ".."))
			{
				continue;
			}
			dwCount++;
			if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
				//递归遍历
				dwCount += CDirInfo::GetObjectCount(dirPath + "\\" + fileData.cFileName);
		}
		FindClose(hFirstFile);
		return dwCount;
	}

	/*!
	* @brief GetObjectNameList	
	* 获取当前目录下所有文件或目录的总数
	* @return DWORD  总数量
	*/
	std::list<std::string> CDirInfo::GetObjectNameList()
	{
		return CDirInfo::GetObjectNameList(m_dirPath);
	}

	/*!
	* @brief GetObjectNameList
	*
	*获取当前目录下所有文件或目录的list
	* @param string  目录名或路径
	* @return std::list<std::string>  总的文件名目录名列表
	*/
	std::list<std::string> CDirInfo::GetObjectNameList(const std::string dirPath)
	{
		std::list<std::string> strNameList;
		if (!Exists(dirPath))
			return strNameList;
		HANDLE hFirstFile;
		WIN32_FIND_DATAA fileData;
		DWORD dwError = 0;
		std::string tempPath = dirPath + "\\*.*";
		hFirstFile = FindFirstFileA(tempPath.c_str(), &fileData);
		if (INVALID_HANDLE_VALUE == hFirstFile)
		{
			return strNameList;
		}
		while (true)
		{
			if (!FindNextFileA(hFirstFile, &fileData) != 0)
			{
				dwError = GetLastError();
				if (dwError == ERROR_NO_MORE_FILES)
					break;
				break;
			} 
			
			if (!strcmp(fileData.cFileName, ".") || !strcmp(fileData.cFileName, ".."))
			{
				continue;
			}
			strNameList.push_back(fileData.cFileName);
			if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				//递归遍历
			{
				std::list<std::string>::iterator  iter = strNameList.end();
				strNameList.splice(iter, CDirInfo::GetObjectNameList(dirPath + "\\" + fileData.cFileName));
			}
				
		}
		FindClose(hFirstFile);
		return strNameList;
	}

	/*!
	* @brief GetAvailableDriver
	*
	* 获取系统上有效的驱动器盘符
	* @return std::list<std::string>
	*/
	std::list<std::string> CDirInfo::GetAvailableDriver()
	{
		return std::list<std::string>();
	}

	/*!
	* @brief Exists
	*
	* 获取当前目录是否存在
	* @return bool  存在为true
	*/
	bool CDirInfo::Exists()
	{
		if (INVALID_FILE_ATTRIBUTES == GetFileAttributesA(m_dirPath.c_str()))
			return false;
		return true;
	}

	/*!
	* @brief GetFilePath
	*
	* 返回文件完整路径
	* @param string  文件名
	* @return string 
	*/
	std::string CDirInfo::GetFilePath(std::string fileName)
	{
		return std::string();
	}

	/*!
	* @brief Mkdir
	*
	* 在当前目录中创建当前指定子目录
	* @return bool  目录存在为true,否则为false
	*/
	bool CDirInfo::Mkdir(std::string dirName)
	{
		std::string path = m_dirPath + "\\" + dirName;
		bool flag = CreateDirectoryA(path.c_str(), NULL);
		return flag;
	}

	/*!
	* @brief MkPath
	*
	* 创建指定路径目录，如父目录不存在，则逐级创建
	* @return bool  目录存在为true,否则为false
	*/
	bool CDirInfo::MkPath(std::string dirPath)
	{
		std::string dir =GetCanonicalizePath(dirPath);
		if (dir.length() == 0)
		{
			return false;
		}
		char *path = new char[dir.length() + 1]();
		sprintf_s(path, dir.length() + 1,"%s", dir.c_str());
		PathRemoveBlanksA(path);
		PathRemoveBackslashA(path);
		std::string strPath = path;
		char  *tag = NULL;
		int iPos = strPath.find("\\", 0);
		//创建父目录
		while (iPos != std::string::npos)
		{
			
			std::string temp = strPath.substr(0,iPos);
			if (!PathIsDirectoryA(temp.c_str()))
			{
				bool flag = CreateDirectoryA(temp.c_str(), NULL);
				if (!flag)
					return flag;
			}
			
			
			iPos = strPath.find("\\",iPos + 1);
		}

		//最后一个目录直接创建
		if (!PathIsDirectoryA(strPath.c_str()))
		{
			bool flag = CreateDirectoryA(strPath.c_str(), NULL);
			if (!flag)
				return flag;
		}
	}


	/*!
	* @brief Mkdir
	*
	* 删除当前目录下指定文件
	* @param string  文件名
	* @return bool 
	*/
	bool CDirInfo::RemoveFile(std::string fileName)
	{
		std::string file = m_dirPath + "\\" + fileName;
		return remove(file.c_str());
	}


	/*!
	* @brief RemoveDir
	*
	* 完整移出一个子目录及其内部文件
	* @return bool
	*/
	bool CDirInfo::RemoveDir(std::string dirName)
	{
		return false;
	}

	/*!
	* @brief RemovePath
	*
	* 完整移除一个目录，及其子目录文件
	* @return bool 
	*/
	bool CDirInfo::RemovePath(std::string dirPath)
	{
		std::list<std::string> strNameList;
		if (!Exists(dirPath))
			return true;
		HANDLE hFirstFile;
		WIN32_FIND_DATAA fileData;
		DWORD dwError = 0;
		std::string tempPath = dirPath + "\\*.*";
		hFirstFile = FindFirstFileA(tempPath.c_str(), &fileData);
		if (INVALID_HANDLE_VALUE == hFirstFile)
		{
			//直接删除当前目录即可
			return RemoveDirectoryA(tempPath.c_str());
		}
		while (true)
		{
			if (!FindNextFileA(hFirstFile, &fileData) != 0)
			{
				dwError = GetLastError();
				if (dwError == ERROR_NO_MORE_FILES)
					break;
				break;
			}

			if (!strcmp(fileData.cFileName, ".") || !strcmp(fileData.cFileName, ".."))
			{
				continue;
			}
			strNameList.push_back(fileData.cFileName);
			if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				//递归遍历
			{
				std::list<std::string>::iterator  iter = strNameList.end();
				CDirInfo::RemovePath(dirPath + "\\" + fileData.cFileName);
			}
			else
			{
				//文件直接删除
				std::string filePath = dirPath + "\\" + fileData.cFileName;
				DeleteFileA(filePath.c_str());
			}

		}
		FindClose(hFirstFile);
		//最后删除当前目录自身		
		return RemoveDirectoryA(dirPath.c_str());
	}

	/*!
	* @brief GetCanonicalizePath
	*
	* 返回一个纯净的目录路径
	* @return bool  存在为true
	*/
	std::string	 CDirInfo::GetCanonicalizePath()
	{
		return CDirInfo::GetCanonicalizePath(m_dirPath);
	}

	/*!
	* @brief GetPathIsRelative
	* @param string  目录名或路径
	* 获取此路径是否相对的，
	*
	* @return bool  相对返回true,绝对返回false
	*/
	bool CDirInfo::GetPathIsRelative(std::string dirPath)
	{
		return PathIsRelativeA(dirPath.c_str());
	}

	/*!
	* @brief GetCanonicalizePath
	*
	* 返回一个纯净的目录路径
	* @return string  
	*/
	std::string CDirInfo::GetCanonicalizePath(std::string dirPath)
	{

		char *tempPath = new char[dirPath.length() + 1]();
		sprintf_s(tempPath, dirPath.length() + 1, "%s", dirPath.c_str());
		PathRemoveBlanksA(tempPath);
		PathRemoveBackslashA(tempPath);
		if (!(GetFileAttributesA(tempPath) & FILE_ATTRIBUTE_DIRECTORY))
		{
			PathRemoveFileSpecA(tempPath);
		}
		char cPath[MAX_PATH] = "";
		if (PathCanonicalizeA(cPath, tempPath))
		{
			return std::string(cPath);
		}
		return std::string();
	}

	/*!
	* @brief GetCurrentAppFullPath
	*
	* 获取当前APP，EXE所在的完整路径，
	*
	* @return string 
	*/
	std::string CDirInfo::GetCurrentAppFullPath()
	{	
		return GetProcessFullPath(GetCurrentProcessId());
	}

	/*!
	* @brief GetCurrentWorkFullPath
	*
	* 当前APP工作目录所在的完整路径
	*
	* @return string  
	*/
	std::string CDirInfo::GetCurrentWorkFullPath()
	{
		//MAX_PATH
		DWORD bufLen = 10;
		DWORD dwError = 0;
		DWORD outLen = 0;
		char *path = new char[bufLen]();
		
		outLen = GetCurrentDirectoryA(bufLen, path);
		if (outLen == 0)
			return std::string();
		if (outLen > bufLen)
		{
			delete[]path;
			path = NULL;
			bufLen = outLen;
			path = new char[bufLen]();
			outLen = GetCurrentDirectoryA(bufLen, path);
			if (outLen != 0)
				return std::string(path);
		}		
		return std::string();
	}

	/*!
	* @brief GetCurrentWorkDir
	*
	* 当前APP工作目录所在目录
	*
	* @return string
	*/
	std::string CDirInfo::GetCurrentWorkDir()
	{		
		return GetPathName(GetCurrentWorkFullPath());
	}

	/*!
	* @brief operator=
	*
	* 对象赋值
	*
	* @return CDirInfo&
	*/
	void CDirInfo::operator=(CDirInfo& dir)
	{
		SaveDirPath(dir.GetPath());
	}

	/*!
	* @brief operator==
	*
	* 对象判断
	*
	* @return bool
	*/
	bool CDirInfo::operator==(CDirInfo& dir)
	{
		return m_dirPath == dir.GetPath();
	}
}

