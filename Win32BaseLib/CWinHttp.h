#pragma once
/*************************************************
*@file  CHttp.h
*@brief 编写与HTTP获取数据有关
**************************************************/
#include <Windows.h>
#include <string>
#include <winhttp.h>
#pragma comment(lib,"Winhttp.lib")
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <list>
#include <vector>
#include <iostream>
/*************************************************
*@class CHttp CHttp.h
*@brief 相关操作HTTP访问
**************************************************/


/*****************************************************************
*HTTP处理类，主要用于HTTP GET/POST、下载（支持重定向）功能
*Author：	JelinYao
*Date：		2015/2/14 12:11
*Email：	mailto://jelinyao@163.com
*/
/*****************************************************************
*/
#pragma once
#include <stdio.h>
#include <tchar.h>
#include <string>
using std::string;
using std::wstring;


enum HttpRequest
{
	HttpGet = 0,
	HttpPost,
};
//枚举下载状态
enum DownloadState
{
	DS_Loading = 0,
	DS_Fialed,
	DS_Finished,
};

/******************************************************
*定义错误信息
*
******************************************************/
enum HttpInterfaceError
{
	HttpErrorSuccess = 0,		//成功
	HttpErrorInit,				//初始化失败
	HttpErrorConnect,			//连接HTTP服务器失败
	HttpErrorSend,				//发送请求失败
	HttpErrorQuery,				//查询HTTP请求头失败
	HttpError404,				//页面不存在
	HttpErrorIllegalUrl,		//无效的URL
	HttpErrorCreateFile,		//创建文件失败
	HttpErrorDownload,			//下载失败
	HttpErrorQueryIP,			//获取域名对应的地址失败
	HttpErrorSocket,			//套接字错误
	HttpErrorUserCancel,		//用户取消下载
	HttpErrorBuffer,			//文件太大，缓冲区不足
	HttpErrorHeader,			//HTTP请求头错误
	HttpErrorParam,				//参数错误，空指针，空字符……
	HttpErrorWriteFile,			//写入文件失败
	HttpErrorUnknow,

};

//下载的回调
class IHttpCallback
{
public:
	virtual void	OnDownloadCallback(void* pParam, DownloadState state, double nTotalSize, double nLoadSize) = 0;
	virtual bool	IsNeedStop() = 0;
};


struct HttpParamsData
{
	void *lpparam;
	IHttpCallback *callback;
	HttpInterfaceError errcode;
};

enum InterfaceType
{
	TypeSocket = 0,
	TypeWinInet,
	TypeWinHttp,
};

//bool CreateInstance(IHttpBase** pBase, InterfaceType flag);
typedef struct _threaddownload
{
	char		 szUrl[MAX_PATH]; //保存所有本次下载需要的URL
	char         szTempFileName[MAX_PATH]; //当前线程保存临时文件路径名
	DWORD        dwId; //第几部份,从1开始
	DWORD        dwStartPos;  //当前线程要开始下载的启点
	DWORD        dwReadLened;//已经下载的长度
	DWORD        dwReadFileLen; //当前线程要下载的长度
	bool		 bIsOk; //是否下载完成
}THREADDOWNLOAD, *PTHREADDOWNLOAD;




//WINHTTP
class CWinHttp
{
public:
	CWinHttp(void);
	virtual ~CWinHttp(void);

	virtual void				SetTimeOut(int dwConnectTime, int dwSendTime, int dwRecvTime);
	virtual std::string			Request(LPCSTR lpUrl, HttpRequest type = HttpGet, LPCSTR lpPostData = NULL, LPCSTR lpHeader = NULL);
	virtual std::string			Request(LPCWSTR lpUrl, HttpRequest type, LPCSTR lpPostData = NULL, LPCWSTR lpHeader = NULL);
	virtual void				FreeInstance() { delete this; }
	virtual bool				DownloadFile(LPCSTR lpUrl, LPCSTR lpFilePath);
	//多线程下载
	virtual bool				DownloadFile(LPCSTR lpUrl, LPCSTR lpFilePath, int iNum);

	virtual bool				DownloadToMem(LPCWSTR lpUrl, OUT void** ppBuffer, OUT int* nSize);
	virtual void				SetDownloadCallback(IHttpCallback* pCallback, void* pParam);
	virtual HttpInterfaceError	GetErrorCode() { return m_paramsData.errcode; }

protected:
	bool	Init();
	void	Release();
	//init
	bool	InitConnect(LPCWSTR lpUrl, HttpRequest type, LPCSTR lpPostData = NULL, LPCWSTR lpHeader = NULL);
	bool	ConnectHttpServer(LPCWSTR lpIP, WORD wPort);
	bool	CreateHttpRequest(LPCWSTR lpPage, HttpRequest type, DWORD dwFlag = 0);
	bool	SendHttpRequest(LPCSTR lpPostData = NULL, LPCWSTR lpHeader = NULL);
	//query 
	bool	QueryRawHeaders(OUT std::wstring& strHeaders);
	bool	QueryContentLength(OUT DWORD& dwLength);


	//分线下载
	//下载指定起点,指定大小文件到本地,线程函数
	static DWORD WINAPI			DownloadThread(__in  LPVOID lpParameter);
	virtual bool				DownloadTempFile(LPCSTR lpUrl, LPCSTR lpFilePath, DWORD start, DWORD readlen);
private:
	bool		m_bHttps;
	HINTERNET	m_hInternet;
	HINTERNET	m_hConnect;
	HINTERNET	m_hRequest;
	int			m_nConnTimeout;
	int			m_nSendTimeout;
	int			m_nRecvTimeout;
	HttpParamsData m_paramsData;
	std::vector<PTHREADDOWNLOAD>   m_ThreadDownInfo;
	static HANDLE		m_hMutex; //线程同步互斥
};





inline void MyParseUrlW(LPCWSTR lpUrl, std::wstring& strHostName, std::wstring& strPage, WORD& sPort)
{
	sPort = 80;
	std::wstring strTemp(lpUrl);
	int nPos = strTemp.find(L"http://");
	if (std::wstring::npos != nPos)
		strTemp = strTemp.substr(nPos + 7, strTemp.size() - nPos - 7);
	else
	{
		nPos = strTemp.find(L"https://");
		if (std::wstring::npos != nPos)
		{
			sPort = 443;//INTERNET_DEFAULT_HTTPS_PORT;
			strTemp = strTemp.substr(nPos + 8, strTemp.size() - nPos - 8);
		}
	}
	nPos = strTemp.find('/');
	if (std::wstring::npos == nPos)//没有找到 /
		strHostName = strTemp;
	else
		strHostName = strTemp.substr(0, nPos);
	int nPos1 = strHostName.find(':');
	if (nPos1 != std::wstring::npos)
	{
		std::wstring strPort = strTemp.substr(nPos1 + 1, strHostName.size() - nPos1 - 1);
		strHostName = strHostName.substr(0, nPos1);
		sPort = (WORD)_wtoi(strPort.c_str());
	}
	if (std::wstring::npos == nPos)
		return;
	strPage = strTemp.substr(nPos, strTemp.size() - nPos);
}

inline void MyParseUrlA(LPCSTR lpUrl, std::string& strHostName, std::string& strPage, WORD& sPort)
{
	sPort = 80;
	std::string strTemp(lpUrl);
	int nPos = strTemp.find("http://");
	if (std::string::npos != nPos)
		strTemp = strTemp.substr(nPos + 7, strTemp.size() - nPos - 7);
	else
	{
		nPos = strTemp.find("https://");
		if (std::wstring::npos != nPos)
		{
			sPort = 443;//INTERNET_DEFAULT_HTTPS_PORT;
			strTemp = strTemp.substr(nPos + 8, strTemp.size() - nPos - 8);
		}
	}
	nPos = strTemp.find('/');
	if (std::string::npos == nPos)//没有找到 /
		strHostName = strTemp;
	else
		strHostName = strTemp.substr(0, nPos);
	int nPos1 = strHostName.find(':');
	if (nPos1 != std::string::npos)
	{
		std::string strPort = strTemp.substr(nPos1 + 1, strHostName.size() - nPos1 - 1);
		strHostName = strHostName.substr(0, nPos1);
		sPort = (WORD)atoi(strPort.c_str());
	}
	if (std::string::npos == nPos)
		return;
	strPage = strTemp.substr(nPos, strTemp.size() - nPos);
}

inline std::wstring Utf2U(const std::string& strUtf8)
{
	std::wstring wstrRet(L"");
	int nLen = MultiByteToWideChar(CP_UTF8, 0, strUtf8.c_str(), -1, NULL, 0);
	if (nLen == ERROR_NO_UNICODE_TRANSLATION)
		throw "Utf8ToUnicode出错：无效的UTF-8字符串。";
	wstrRet.resize(nLen + 1, '\0');
	MultiByteToWideChar(CP_UTF8, 0, strUtf8.c_str(), -1, (LPWSTR)wstrRet.c_str(), nLen);
	return wstrRet;
}

inline std::string U2A(const std::wstring& str)
{
	std::string strDes;
	if (str.empty())
		goto __end;
	int nLen = ::WideCharToMultiByte(CP_ACP, 0, str.c_str(), str.size(), NULL, 0, NULL, NULL);
	if (0 == nLen)
		goto __end;
	char* pBuffer = new char[nLen + 1];
	memset(pBuffer, 0, nLen + 1);
	::WideCharToMultiByte(CP_ACP, 0, str.c_str(), str.size(), pBuffer, nLen, NULL, NULL);
	pBuffer[nLen] = '\0';
	strDes.append(pBuffer);
	delete[] pBuffer;
__end:
	return strDes;
}

inline std::wstring A2U(const std::string& str)
{
	std::wstring strDes;
	if (str.empty())
		goto __end;
	int nLen = ::MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), NULL, 0);
	if (0 == nLen)
		goto __end;
	wchar_t* pBuffer = new wchar_t[nLen + 1];
	memset(pBuffer, 0, nLen + 1);
	::MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), pBuffer, nLen);
	pBuffer[nLen] = '\0';
	strDes.append(pBuffer);
	delete[] pBuffer;
__end:
	return strDes;
}

inline bool FileExistA(LPCSTR lpFile)
{
	HANDLE hFile = CreateFileA(lpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return false;
	CloseHandle(hFile);
	return true;
}

inline bool FileExistW(LPCWSTR lpFile)
{
	HANDLE hFile = CreateFileW(lpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return false;
	CloseHandle(hFile);
	return true;
}
