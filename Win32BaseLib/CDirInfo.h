
#pragma once
#include <string>
#include <shlwapi.h>
#include <Windows.h>
#include <list>
#pragma comment(lib,"shlwapi.lib")
namespace Win32BaseLib
{
	class CDirInfo
	{
	public:
		CDirInfo();
		CDirInfo(std::string& dirPath = std::string());
		CDirInfo(std::string dirPath = std::string());
		void							SetPath(std::string dirPath);
		std::string						GetPath();
		~CDirInfo();
		std::string						GetAbsoluteFilePath(std::string fileName);
		std::string						GetRelativeFilePath(std::string fileName);
		bool							Cd(std::string newDirPath);
		bool							CdUp();
		static std::string				CdUp(std::string dirPath);
		DWORD							GetObjectCount();
		static DWORD					GetObjectCount(const std::string dirPath);
		std::list<std::string>			GetObjectNameList();
		static std::list<std::string>	GetObjectNameList(const std::string dirPath);
		std::list<std::string>			GetAvailableDriver();
		bool							Exists();
		std::string						GetFilePath(std::string fileName);
		bool							Mkdir(std::string dirName);
		bool							MkPath(std::string dirPath);
		bool							RemoveFile(std::string fileName);
		bool							RemoveDir(std::string dirName);
		static bool						RemovePath(std::string dirPath);
		std::string						GetCanonicalizePath();
		static bool						GetPathIsRelative(std::string dirPath);
		static std::string				GetCanonicalizePath(std::string dirPath);
		static std::string				GetCurrentAppFullPath();
		static std::string				GetCurrentWorkFullPath();
		static std::string				GetCurrentWorkDir();
		static std::string				GetCurrentAppDir();
		static std::string				GetProcessFullPath(DWORD pid);
		static std::string				GetProcessDir(DWORD pid);
		static bool						Exists(std::string dirPath);
		static std::string				GetSysPath(std::string sysName);
		static bool						SetCurrentAppWorkPath(std::string dirPath);
		static std::string				GetPathName(std::string dirPath);
		static bool						CopyDirTo(std::string ddirPath, std::string sdirPath);
		void							operator=(CDirInfo& dir);
		bool							operator==(CDirInfo& dir);
	private:
		bool							SaveDirPath(std::string dirPath);

		std::string						m_dirPath;		
	};
}