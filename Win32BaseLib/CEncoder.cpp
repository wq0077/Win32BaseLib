//#include "stdafx.h"
#include "CEncoder.h"
#include <string>
#include <assert.h>
#include <iostream>
#include <algorithm>
#include "base\md5.h"
using namespace std;

namespace Win32BaseLib
{
	
	
	/*!
	* @brief Md5Str
	*
	* 获取字符串的MD5值
	* @param string str 原文
	* @param bool isUpper 大写
	* @param bool is16 是否返回16位字符串
	* @return string  MD5值
	*/
	std::string CEncoder::Md5Str(std::string str,bool isUpper,bool is16)
	{
		std::string md5Str = md5(str);
		if (isUpper)//大写
		{
			transform(md5Str.begin(), md5Str.end(), md5Str.begin(), ::toupper); //转换成大写
		}
		if (is16)
		{
			return md5Str.substr(8,16);
		}
		return md5Str;
	}
	/*!
	* @brief Md5File
	*
	* 获取文件的MD5值
	* @param string filePath 文件完整路径
	* @param bool	isUpper 大写
	* @param bool	is16 是否返回16位字符串
	* @return string  MD5值
	*/
	std::string CEncoder::Md5File(std::string filePath, bool isUpper, bool is16)
	{
		std::string md5Str = md5file(filePath.c_str());
		if (isUpper)//大写
		{
			transform(md5Str.begin(), md5Str.end(), md5Str.begin(), ::toupper); //转换成大写
		}
		if (is16)
		{
			return md5Str.substr(8, 16);
		}
		return md5Str;
	}

	/*!
	* @brief Base64Encode
	*
	* 对明文使用BASE64编码
	* @param bin_data string    原文
	* @return string  编码
	*/
	std::string  CEncoder::Base64Encode(std::string bin_data)
	{
		const char *base64char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		int i, j;
		unsigned char current;
		int bin_length = bin_data.length();
		if (bin_length == 0)
		{
			return std::string();
		}
		char *base64 = new char[bin_length * 4]();
		for (i = 0, j = 0; i < bin_length; i += 3)
		{
			current = (bin_data[i] >> 2);
			current &= (unsigned char)0x3F;
			base64[j++] = base64char[(int)current];

			current = ((unsigned char)(bin_data[i] << 4)) & ((unsigned char)0x30);
			if (i + 1 >= bin_length)
			{
				base64[j++] = base64char[(int)current];
				base64[j++] = '=';
				base64[j++] = '=';
				break;
			}
			current |= ((unsigned char)(bin_data[i + 1] >> 4)) & ((unsigned char)0x0F);
			base64[j++] = base64char[(int)current];

			current = ((unsigned char)(bin_data[i + 1] << 2)) & ((unsigned char)0x3C);
			if (i + 2 >= bin_length)
			{
				base64[j++] = base64char[(int)current];
				base64[j++] = '=';
				break;
			}
			current |= ((unsigned char)(bin_data[i + 2] >> 6)) & ((unsigned char)0x03);
			base64[j++] = base64char[(int)current];

			current = ((unsigned char)bin_data[i + 2]) & ((unsigned char)0x3F);
			base64[j++] = base64char[(int)current];
		}
		base64[j] = '\0';
		return base64;
	}

	/*!
	* @brief Base64Decode
	*
	* 对BASE64字符串解码
	* @param base64 string  base64
	* @return string  原文
	*/
	std::string  CEncoder::Base64Decode(std::string  base64)
	{
		const char *base64char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		int i, j;
		unsigned char k;
		unsigned char temp[4];
		int baseLen = base64.length();
		if (baseLen == 0)
		{
			return std::string();
		}
		char *bindata = new char[baseLen]();
		for (i = 0, j = 0; base64[i] != '\0'; i += 4)
		{
			memset(temp, 0xFF, sizeof(temp));
			for (k = 0; k < 64; k++)
			{
				if (base64char[k] == base64[i])
					temp[0] = k;
			}
			for (k = 0; k < 64; k++)
			{
				if (base64char[k] == base64[i + 1])
					temp[1] = k;
			}
			for (k = 0; k < 64; k++)
			{
				if (base64char[k] == base64[i + 2])
					temp[2] = k;
			}
			for (k = 0; k < 64; k++)
			{
				if (base64char[k] == base64[i + 3])
					temp[3] = k;
			}

			bindata[j++] = ((unsigned char)(((unsigned char)(temp[0] << 2)) & 0xFC)) |
				((unsigned char)((unsigned char)(temp[1] >> 4) & 0x03));
			if (base64[i + 2] == '=')
				break;

			bindata[j++] = ((unsigned char)(((unsigned char)(temp[1] << 4)) & 0xF0)) |
				((unsigned char)((unsigned char)(temp[2] >> 2) & 0x0F));
			if (base64[i + 3] == '=')
				break;

			bindata[j++] = ((unsigned char)(((unsigned char)(temp[2] << 6)) & 0xF0)) |
				((unsigned char)(temp[3] & 0x3F));
		}
		return bindata;
	}

	/*!
	* @brief UrlEncode
	*
	* 对明文使用URL编码
	* @param  string    原文
	* @return string  编码
	*/
	string CEncoder::UrlEncode(const string &str)
	{
		string strResult;
		size_t nLength = str.length();
		unsigned char* pBytes = (unsigned char*)str.c_str();
		char szAlnum[2];
		char szOther[4];
		for (size_t i = 0; i < nLength; i++)
		{
			if (isalnum((BYTE)str[i]))
			{
				sprintf_s(szAlnum, sizeof(szAlnum), "%c", str[i]);
				strResult.append(szAlnum);
			}
			else if (isspace((BYTE)str[i]))
			{
				strResult.append("+");
			}
			else
			{
				sprintf_s(szOther, sizeof(szOther), "%%%X%X", pBytes[i] >> 4, pBytes[i] % 16);
				strResult.append(szOther);
			}
		}
		return strResult;
	}

	/*!
	* @brief UrlDecode
	*
	* 对明文使用URL解码
	* @param  string    编码
	* @return string  原文
	*/
	string CEncoder::UrlDecode(const string &str)
	{
		string strResult;
		char szTemp[2];
		size_t i = 0;
		size_t nLength = str.length();
		while (i < nLength)
		{
			if (str[i] == '%')
			{
				szTemp[0] = str[i + 1];
				szTemp[1] = str[i + 2];
				strResult += StrToBin(szTemp);
				i = i + 3;
			}
			else if (str[i] == '+')
			{
				strResult += ' ';
				i++;
			}
			else
			{
				strResult += str[i];
				i++;
			}
		}
		return strResult;
	}

	/*!
	* @brief UTF8UrlEncode
	*
	* 对明文使用UTF8_URL编码
	* @param  string    原文
	* @return string  编码
	*/
	string CEncoder::UTF8UrlEncode(const string &str)
	{
		return UrlEncode(AnsiStringToUTF8String(str));
	}

	/*!
	* @brief UTF8UrlDecode
	*
	* 对明文使用UTF8_URL解码
	* @param  string    编码
	* @return string  原文
	*/
	string CEncoder::UTF8UrlDecode(const string &str)
	{
		return UTF8StringToAnsiString(UrlDecode(str));
	}

	/*!
	* @brief UTF8ToANSI
	*
	* UTF8转ANSI
	* @param  string    UTF8格式字符串
	* @return string  ANSI字符串
	*/
	std::string CEncoder::UTF8ToANSI(const std::string & str)
	{
		std::string szAnsi = "";
		UINT nLen = MultiByteToWideChar(CP_UTF8, NULL, str.c_str(), -1, NULL, NULL);
		WCHAR *wszBuffer = new WCHAR[nLen + 1];
		nLen = MultiByteToWideChar(CP_UTF8, NULL, str.c_str(), -1, wszBuffer, nLen);
		wszBuffer[nLen] = 0;

		nLen = WideCharToMultiByte(CP_ACP, NULL, wszBuffer, -1, NULL, NULL, NULL, NULL);
		CHAR *szBuffer = new CHAR[nLen + 1];
		nLen = WideCharToMultiByte(CP_ACP, NULL, wszBuffer, -1, szBuffer, nLen, NULL, NULL);
		szBuffer[nLen] = 0;
		if (nLen > 0 && strlen(szBuffer) > 0)
			szAnsi = szBuffer;
		if (szBuffer)
			delete[]szBuffer;
		if (wszBuffer)
			delete[]wszBuffer;

		return szAnsi;
	}

	/*!
	* @brief ANSIToUTF8
	*
	* ANSI转UTF8
	* @param  string   ANSI字符串  
	* @return string   UTF8格式字符串
	*/
	std::string CEncoder::ANSIToUTF8(const std::string & str)
	{
		std::string strUtf8 = "";
		//获取转换为宽字节后需要的缓冲区大小，创建宽字节缓冲区，936为简体中文GB2312代码页
		UINT nLen = MultiByteToWideChar(936, NULL, str.c_str(), -1, NULL, NULL);
		WCHAR *wszBuffer = new WCHAR[nLen + 1];
		nLen = MultiByteToWideChar(936, NULL, str.c_str(), -1, wszBuffer, nLen);
		wszBuffer[nLen] = 0;
		//获取转为UTF8多字节后需要的缓冲区大小，创建多字节缓冲区
		nLen = WideCharToMultiByte(CP_UTF8, NULL, wszBuffer, -1, NULL, NULL, NULL, NULL);
		CHAR *szBuffer = new CHAR[nLen + 1];
		nLen = WideCharToMultiByte(CP_UTF8, NULL, wszBuffer, -1, szBuffer, nLen, NULL, NULL);
		szBuffer[nLen] = 0;
		if (nLen > 0 && strlen(szBuffer) > 0)
			strUtf8 = szBuffer;
		if (wszBuffer)
			delete[]wszBuffer;
		if (szBuffer)
			delete[]szBuffer;
		return strUtf8;
	}

	/*!
	* @brief UTF8ToANSI
	*
	* UTF8转ANSI
	* @param  string    UTF8格式字符串
	* @return string  ANSI字符串
	*/
	string CEncoder::UTF8StringToAnsiString(const string &strUtf8)
	{
		string strResult;
		int nUTF8StringLength = strUtf8.length();
		int nResultLength = nUTF8StringLength + (nUTF8StringLength >> 2) + 2;
		strResult.resize(nResultLength);
		int i = 0;
		int j = 0;
		char szBuffer[4] = { 0 };
		WCHAR cchWideChar;
		while (i < nUTF8StringLength)
		{
			if (strUtf8[i] >= 0)
			{
				strResult[j++] = strUtf8[i++];
			}
			else
			{
				UTF8CharToUnicodeChar(&cchWideChar, &strUtf8[i]);
				UnicodeToAnsi(szBuffer, 2, &cchWideChar, 1);

				strResult[j] = szBuffer[0];
				strResult[j + 1] = szBuffer[1];
				strResult[j + 2] = szBuffer[2];
				i += 3;
				j += 2;
			}
		}
		return strResult;
	}

	/*!
	* @brief ANSIToUTF8
	*
	* ANSI转UTF8
	* @param  string   ANSI字符串
	* @return string   UTF8格式字符串
	*/
	string CEncoder::AnsiStringToUTF8String(const string& strAnsi)
	{
		string strResult;
		int nAnsiStringLength = strAnsi.length();
		char szBuffer[4] = { 0 };
		strResult.clear();
		int i = 0;
		char szAscii[2] = { 0 };
		WCHAR cchWideChar;
		while (i < nAnsiStringLength)
		{
			if (strAnsi[i] >= 0)
			{
				szAscii[0] = (strAnsi[i++]);
				strResult.append(szAscii);
			}
			else
			{
				AnsiToUnicode(&cchWideChar, 1, &strAnsi[i], 2);
				UnicodeCharToUTF8Char(szBuffer, &cchWideChar);
				strResult.append(szBuffer);
				i += 2;
			}
		}
		return strResult;
	}


	void CEncoder::AnsiToUnicode(WCHAR* pUnicodeBuffer, int nUnicodeBufferSize, const char *pAnsiBuffer, int nAnsiBufferSize)
	{
		MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, pAnsiBuffer, nAnsiBufferSize, pUnicodeBuffer, nUnicodeBufferSize);
	}

	void CEncoder::UnicodeToAnsi(char* pAnsiBuffer, int nAnsiBufferSize, WCHAR* pUnicodeBuffer, int nUnicodeBufferSize)
	{
		WideCharToMultiByte(CP_ACP, NULL, pUnicodeBuffer, nUnicodeBufferSize, pAnsiBuffer, nAnsiBufferSize, NULL, NULL);
	}

	void CEncoder::UTF8CharToUnicodeChar(WCHAR* pUnicodeBuffer, const char *pUTF8Buffer)
	{
		char* pChar = (char *)pUnicodeBuffer;
		pChar[1] = ((pUTF8Buffer[0] & 0x0F) << 4) + ((pUTF8Buffer[1] >> 2) & 0x0F);
		pChar[0] = ((pUTF8Buffer[1] & 0x03) << 6) + (pUTF8Buffer[2] & 0x3F);
	}

	void CEncoder::UnicodeCharToUTF8Char(char* pUTF8Buffer, const WCHAR* pUnicodeBuffer)
	{
		const char* pChar = (const char *)pUnicodeBuffer;
		pUTF8Buffer[0] = (0xE0 | ((pChar[1] & 0xF0) >> 4));
		pUTF8Buffer[1] = (0x80 | ((pChar[1] & 0x0F) << 2)) + ((pChar[0] & 0xC0) >> 6);
		pUTF8Buffer[2] = (0x80 | (pChar[0] & 0x3F));
	}

	char CEncoder::CharToInt(char ch)
	{
		if (ch >= '0' && ch <= '9')
		{
			return (char)(ch - '0');
		}
		if (ch >= 'a' && ch <= 'f')
		{
			return (char)(ch - 'a' + 10);
		}
		if (ch >= 'A' && ch <= 'F')
		{
			return (char)(ch - 'A' + 10);
		}
		return -1;
	}

	char CEncoder::StrToBin(char *pString)
	{
		char szBuffer[2];
		char ch;
		szBuffer[0] = CharToInt(pString[0]); //make the B to 11 -- 00001011 
		szBuffer[1] = CharToInt(pString[1]); //make the 0 to 0 -- 00000000 
		ch = (szBuffer[0] << 4) | szBuffer[1]; //to change the BO to 10110000 
		return ch;
	}

	/*!
	* @brief string2wstring
	*
	* string 转 wstring
	* @param  string   string字符串
	* @return wstring  wstring格式字符串
	*/
	std::wstring CEncoder::string2wstring(std::string & str)
	{
		wstring result;
		//获取缓冲区大小，并申请空间，缓冲区大小按字符计算  
		int len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), NULL, 0);
		WCHAR* buffer = new WCHAR[len + 1];
		//多字节编码转换成宽字节编码  
		MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), buffer, len);
		buffer[len] = '\0';             //添加字符串结尾  
										//删除缓冲区并返回值  
		result.append(buffer);
		delete[] buffer;
		return result;
	}


	/*!
	* @brief wstring2string
	*
	* wstring 转 string
	* @param  wstring  wstring 字符串
	* @return string   string格式字符串
	*/	
	std::string CEncoder::wstring2string(std::wstring& wstr)
	{
		string result;
		//获取缓冲区大小，并申请空间，缓冲区大小事按字节计算的  
		int len = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), NULL, 0, NULL, NULL);
		char* buffer = new char[len + 1];
		//宽字节编码转换成多字节编码  
		WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), buffer, len, NULL, NULL);
		buffer[len] = '\0';
		//删除缓冲区并返回值  
		result.append(buffer);
		delete[] buffer;
		return result;
	}

	/*!
	* @brief Utf8Code2String
	*
	* 将UTF8\UXXX 字符 转换为实际字符串
	* @param  char  \UXXX字符串
	* @return string   string原文
	*/
	std::string CEncoder::Utf8Code2String(char* szCode)
	{
		return std::string(szCode);
		string strRet = "";
		char szDelimiters[] = "\\u";
		char* pTok = NULL;
		pTok = strtok_s(szCode, szDelimiters, &pTok);
		while (pTok != NULL) {
			/*OutputDebugString(pTok);
			OutputDebugString("\r\n");*/
			if (strlen(pTok) >= 4)
			{
				string strRet = "";
				for (int i = 0; i < 4; i++)
				{
					if (szCode[i] >= '0' && szCode[i] <= '9')	continue;
					if (szCode[i] >= 'A' && szCode[i] <= 'F')	continue;
					if (szCode[i] >= 'a' && szCode[i] <= 'f')	continue;
					return strRet;
				}

				char unicode_hex[5] = { 0 };
				memcpy(unicode_hex, szCode, 4);
				unsigned int iCode = 0;
				sscanf_s(unicode_hex, "%04x", &iCode);
				wchar_t wchChar[4] = { 0 };
				wchChar[0] = iCode;

				char szAnsi[8] = { 0 };
				WideCharToMultiByte(CP_ACP, NULL, wchChar, 1, szAnsi, sizeof(szAnsi), NULL, NULL);
				strRet = string(szAnsi);

				strRet += strRet;
			}
			pTok = strtok_s(NULL, szDelimiters, &pTok);
		}
		return strRet;
	}
}
