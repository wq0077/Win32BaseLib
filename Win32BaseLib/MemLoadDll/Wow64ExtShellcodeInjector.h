#pragma once
#include "ReflectiveLoaderShellcodeX64.h"
#include "ReflectiveLoaderShellcodeX86.h"

#include "wow64ext.h"
#include <tchar.h>
#include <windows.h>
#include <stdio.h>
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")


#define SE_DEBUG_PRIVILEGE                (20L)

typedef
NTSTATUS(NTAPI * pfnRtlAdjustPrivilege)(
	UINT32 Privilege,
	BOOLEAN Enable,
	BOOLEAN Client,
	PBOOLEAN WasEnabled);

BOOL GrantPriviledge(IN UINT32 Priviledge);
BOOL IsPE64(IMAGE_NT_HEADERS * pNTH);
BOOL IsPE64File(char* lpDllPath);
BOOL IsPE64FileFromData(char* lpData);
BOOL ReadFileData(char *filename, BYTE **buff, DWORD *size);
BOOL ConstructShellcode(char *filename, BYTE **buff, DWORD *size);
BOOL ConstructShellcodeFromData(char *lpData, DWORD dwData, BYTE **buff, DWORD *size);
BOOL IsOs64();
BOOL Is32BitProcess(DWORD dwProcessId);
BOOL InjectFromWow64ShellcodeToProcess(HANDLE hProcess, LPVOID lpShellcode, DWORD dwSize);
BOOL InjectShellcodeToProcess(DWORD dwProcessId, LPVOID lpShellcode, DWORD dwSize);
BOOL IsTargetProcessArchWithDllMatch(DWORD dwTargetProcessId, char* lpDllPath);
