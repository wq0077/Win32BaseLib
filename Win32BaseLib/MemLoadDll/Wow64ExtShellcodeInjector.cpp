// Wow64ExtShellcodeInjector.cpp : 定义控制台应用程序的入口点。
//

#include "Wow64ExtShellcodeInjector.h"

	
BOOL GrantPriviledge(IN UINT32 Priviledge)
{		
	pfnRtlAdjustPrivilege    RtlAdjustPrivilege = NULL;
	BOOLEAN                    WasEnable = FALSE;

	RtlAdjustPrivilege = (pfnRtlAdjustPrivilege)GetProcAddress(GetModuleHandle("ntdll.dll"), "RtlAdjustPrivilege");
	if (RtlAdjustPrivilege == NULL)
	{
		return FALSE;
	}

	RtlAdjustPrivilege(Priviledge, TRUE, FALSE, &WasEnable);

	return TRUE;
}
	
BOOL IsPE64(IMAGE_NT_HEADERS * pNTH)
{		
	return (pNTH->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR64_MAGIC);
}

BOOL IsPE64File(char* lpDllPath)
{
	BOOL bRet = FALSE;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	BYTE Buffer[1000] = {0};
	DWORD dwNumberOfBytesRead = 0;
	PIMAGE_DOS_HEADER lpDosHeader = NULL;
	PIMAGE_NT_HEADERS lpNtHeader = NULL;

	do 
	{
		if (!lpDllPath)
		{
			break;
		}

		hFile = CreateFile(
			lpDllPath,
			GENERIC_READ,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL
			);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			break;
		}

		if (!ReadFile(
			hFile,
			Buffer,
			1000,
			&dwNumberOfBytesRead,
			NULL
			))
		{
			break;
		}

		if (dwNumberOfBytesRead != 1000)
		{
			break;
		}

		lpDosHeader = (PIMAGE_DOS_HEADER)Buffer;
		if (IsBadReadPtr(lpDosHeader, sizeof(DWORD)))
		{
			break;
		}

		if (lpDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
		{
			break;
		}
		
		lpNtHeader = (PIMAGE_NT_HEADERS)((PBYTE)lpDosHeader + lpDosHeader->e_lfanew);
		if (IsBadReadPtr(lpNtHeader, sizeof(DWORD)))
		{
			break;
		}

		if (lpNtHeader->Signature != IMAGE_NT_SIGNATURE)
		{
			break;
		}

		bRet = IsPE64(lpNtHeader);

	} while (FALSE);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
	}

	return bRet;
}

BOOL IsPE64FileFromData(char* lpData)
{
	BOOL bRet = FALSE;
	PIMAGE_DOS_HEADER lpDosHeader = NULL;
	PIMAGE_NT_HEADERS lpNtHeader = NULL;

	do
	{
		if (!lpData)
			break;

		lpDosHeader = (PIMAGE_DOS_HEADER)lpData;
		if (IsBadReadPtr(lpDosHeader, sizeof(DWORD)))
			break;

		if (lpDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
			break;

		lpNtHeader = (PIMAGE_NT_HEADERS)((PBYTE)lpDosHeader + lpDosHeader->e_lfanew);
		if (IsBadReadPtr(lpNtHeader, sizeof(DWORD)))
		{
			break;
		}

		if (lpNtHeader->Signature != IMAGE_NT_SIGNATURE)
		{
			break;
		}

		bRet = IsPE64(lpNtHeader);

	} while (FALSE);

	return bRet;
}
	
BOOL ReadFileData(char *filename, BYTE **buff, DWORD *size)
{
	if (filename == NULL || buff == NULL || size == NULL)
		return FALSE;

	HANDLE hFile = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;

	// get size of file
	LARGE_INTEGER liSize;
	if (!GetFileSizeEx(hFile, &liSize)) {
		CloseHandle(hFile);
		return FALSE;
	}
	if (liSize.HighPart > 0) {
		CloseHandle(hFile);
		return FALSE;
	}

	// read entire file into memory
	*buff = (BYTE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, liSize.LowPart);
	if (*buff == NULL) {
		CloseHandle(hFile);
		return FALSE;
	}
	BYTE *buffPtr = *buff;
	DWORD numLeft = liSize.LowPart;
	DWORD numRead = 0;
	while (numLeft > 0) {
		if (!ReadFile(hFile, buffPtr, numLeft, &numRead, NULL)) {
			CloseHandle(hFile);
			HeapFree(GetProcessHeap(), 0, *buff);
			return FALSE;
		}
		numLeft -= numRead;
		buffPtr += numRead;
	}
	*size = liSize.LowPart;
	CloseHandle(hFile);
	return TRUE;
}

BOOL ConstructShellcode(char *filename, BYTE **buff, DWORD *size)
{
	BOOL bRet = FALSE;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	PVOID lpDllData = NULL;
	DWORD dwDllSize = 0;
	PBYTE lpTemp = NULL;
	BOOL bReadFile = TRUE;
	PIMAGE_DOS_HEADER lpDosHeader = NULL;
	PIMAGE_NT_HEADERS lpNtHeader = NULL;

	do 
	{
		if (filename == NULL || buff == NULL || size == NULL)
			break;

		hFile = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
			break;

		// get size of file
		LARGE_INTEGER liSize;
		if (!GetFileSizeEx(hFile, &liSize))
			break;	

		if (liSize.HighPart > 0)
			break;

		// read entire file into memory
		lpDllData = (BYTE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, liSize.LowPart);
		if (lpDllData == NULL)
			break;
		
		DWORD numLeft = liSize.LowPart;
		DWORD numRead = 0;

		lpTemp = (PBYTE)lpDllData;

		while (numLeft > 0) {

			if (!ReadFile(hFile, lpDllData, numLeft, &numRead, NULL))
			{
				bReadFile = FALSE;
				break;
			}

			numLeft -= numRead;
			lpTemp += numRead;
		}

		if (bReadFile == FALSE)
		{
			break;
		}

		if (hFile != INVALID_HANDLE_VALUE)
		{
			CloseHandle(hFile);
			hFile = INVALID_HANDLE_VALUE;
		}

		if (IsPE64File(filename))
		{	
			*buff = (BYTE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, liSize.LowPart + sizeof(ReflectiveLoaderShellcodeX64));
			if (*buff == NULL)
				break;
			
			*size = liSize.LowPart + sizeof(ReflectiveLoaderShellcodeX64);

			memcpy(*buff, (PVOID)ReflectiveLoaderShellcodeX64, sizeof(ReflectiveLoaderShellcodeX64));
			memcpy(*buff + sizeof(ReflectiveLoaderShellcodeX64), lpDllData, liSize.LowPart);
		}
		else
		{
			*buff = (BYTE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, liSize.LowPart + sizeof(ReflectiveLoaderShellcodeX86));
			if (*buff == NULL)
				break;
			
			*size = liSize.LowPart + sizeof(ReflectiveLoaderShellcodeX86);

			memcpy(*buff, (PVOID)ReflectiveLoaderShellcodeX86, sizeof(ReflectiveLoaderShellcodeX86));
			memcpy(*buff + sizeof(ReflectiveLoaderShellcodeX86), lpDllData, liSize.LowPart);
		}
		
		bRet = TRUE;

	} while (FALSE);

	if (lpDllData)
	{
		HeapFree(GetProcessHeap(), 0, lpDllData);
	}

	if (hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
	}

	return bRet;
}

BOOL ConstructShellcodeFromData(char *lpData,DWORD dwData, BYTE **buff, DWORD *size)
{
	BOOL bRet = FALSE;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	PVOID lpDllData = NULL;
	DWORD dwDllSize = 0;
	PBYTE lpTemp = NULL;
	BOOL bReadFile = TRUE;
	PIMAGE_DOS_HEADER lpDosHeader = NULL;
	PIMAGE_NT_HEADERS lpNtHeader = NULL;

	do
	{

		if (IsPE64FileFromData(lpData))
		{
			*buff = (BYTE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwData + sizeof(ReflectiveLoaderShellcodeX64));
			if (*buff == NULL)
				break;

			*size = dwData + sizeof(ReflectiveLoaderShellcodeX64);

			memcpy(*buff, (PVOID)ReflectiveLoaderShellcodeX64, sizeof(ReflectiveLoaderShellcodeX64));
			memcpy(*buff + sizeof(ReflectiveLoaderShellcodeX64), lpData, dwData);
		}
		else
		{
			*buff = (BYTE*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwData + sizeof(ReflectiveLoaderShellcodeX86));
			if (*buff == NULL)
				break;

			*size = dwData + sizeof(ReflectiveLoaderShellcodeX86);

			memcpy(*buff, (PVOID)ReflectiveLoaderShellcodeX86, sizeof(ReflectiveLoaderShellcodeX86));
			memcpy(*buff + sizeof(ReflectiveLoaderShellcodeX86), lpData, dwData);
		}

		bRet = TRUE;

	} while (FALSE);

	//if (lpData)
	//{
	//	HeapFree(GetProcessHeap(), 0, lpData);
	//}


	return bRet;
}



BOOL IsOs64()
{
	SYSTEM_INFO si;
	GetNativeSystemInfo(&si);
	if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64
		|| si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64) {
			return TRUE;
	}
	else {
		return FALSE;
	}

}
	
BOOL Is32BitProcess(DWORD dwProcessId)
{
	// 如果不是64位统，则肯定是32位进程
	if (!IsOs64()) {
		return TRUE;
	}

	if (dwProcessId == 0 || dwProcessId == 4) {
		return FALSE;
	}
	
	// 判断是否是32位系统
	typedef BOOL(WINAPI * LPFN_ISWOW64PROCESS)(HANDLE, PBOOL);
	static LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle(_T("kernel32")), "IsWow64Process");
	// 是32位系统, 进程止定是 32位的
	if (!fnIsWow64Process) {
		return TRUE;
	}

	// 判断是否是 64位系统下面的 32位进程
	HANDLE h = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcessId);
	if (!h || h == INVALID_HANDLE_VALUE) {
		return FALSE;
	}
	
	BOOL b32 = 0;
	BOOL b = fnIsWow64Process(h, &b32);
	
	CloseHandle(h);
	return b32;
}
		
BOOL InjectFromWow64ShellcodeToProcess(HANDLE hProcess, LPVOID lpShellcode, DWORD dwSize)
{
	BOOL bRet = FALSE;
	LPVOID lpRemoteCodeBuffer = NULL;
	BOOL bWritten = FALSE;
		
	do 
	{
		lpRemoteCodeBuffer = VirtualAllocEx(
			hProcess,
			NULL,
			dwSize,
			MEM_RESERVE | MEM_COMMIT,
			PAGE_EXECUTE_READWRITE
			);

		if (lpRemoteCodeBuffer == NULL)
		{
			break;
		}

		bWritten = WriteProcessMemory(
			hProcess,
			lpRemoteCodeBuffer,
			lpShellcode,
			dwSize,
			NULL
			);

		if (!bWritten)
		{
			break;
		}

		DWORD64 hThread64 = MyCreateRemoteThread64(
			(DWORD64)hProcess,
			(DWORD64)(LPTHREAD_START_ROUTINE)lpRemoteCodeBuffer,
			NULL
			);

		if (hThread64 == NULL)
		{
			break;
		}

		CloseHandle64(hThread64);
		bRet = TRUE;

	} while (FALSE);
	
	return bRet;
}

BOOL InjectShellcodeToProcess(DWORD dwProcessId, LPVOID lpShellcode, DWORD dwSize)
{
	BOOL bRet = FALSE;
	BOOL bTargetProcessWin64 = FALSE;
	HANDLE hProcess = NULL;
	LPVOID lpRemoteCodeBuffer = NULL;
	BOOL bWritten = FALSE;

#ifdef _WIN64
	#error InjectShellcodeToProcess assumes to be running from a 32-bit process
#endif

	do 
	{
		hProcess = OpenProcess(
			PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_READ |PROCESS_VM_WRITE,
			FALSE,
			dwProcessId
			);

		if (hProcess == NULL)
		{
			break;
		}

		BOOL bIsWow64 = FALSE;
		if (!Is32BitProcess(dwProcessId))
		{
			SYSTEM_INFO si = {0};
			GetNativeSystemInfo(&si);
			if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
			{
				bTargetProcessWin64 = TRUE;
			}	
		}
		
		if (bTargetProcessWin64)
		{
			bRet = InjectFromWow64ShellcodeToProcess(hProcess, lpShellcode, dwSize);
		}
		else
		{
			lpRemoteCodeBuffer = VirtualAllocEx(
				hProcess,
				NULL,
				dwSize,
				MEM_RESERVE | MEM_COMMIT,
				PAGE_EXECUTE_READWRITE
				);

			if (lpRemoteCodeBuffer == NULL)
			{
				break;
			}

			bWritten = WriteProcessMemory(
				hProcess,
				lpRemoteCodeBuffer,
				lpShellcode,
				dwSize,
				NULL
				);

			if (!bWritten)
			{
				break;
			}

			HANDLE hThread = CreateRemoteThread(
				hProcess,
				NULL,
				0,
				(LPTHREAD_START_ROUTINE)lpRemoteCodeBuffer,
				NULL,
				0,
				NULL
				);

			if (!hThread)
			{
				break;
			}
			
			CloseHandle(hThread);
			bRet = TRUE;
		}

	} while (FALSE);
	
	if (hProcess != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hProcess);
	}

	return bRet;
}

BOOL IsTargetProcessArchWithDllMatch(DWORD dwTargetProcessId, char* lpDllPath)
{
	BOOL bRet = FALSE;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	BYTE Buffer[1000] = {0};
	DWORD dwNumberOfBytesRead = 0;
	PIMAGE_DOS_HEADER lpDosHeader = NULL;
	PIMAGE_NT_HEADERS lpNtHeader = NULL;
	BOOL bIsPe64 = FALSE;
	BOOL bIs32BitProcess = FALSE;
	
	do 
	{
		if (!dwTargetProcessId || !lpDllPath)
		{
			break;
		}

		hFile = CreateFile(
			lpDllPath,
			GENERIC_READ,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL
			);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			break;
		}

		if (!ReadFile(
			hFile,
			Buffer,
			1000,
			&dwNumberOfBytesRead,
			NULL
			))
		{
			break;
		}

		if (dwNumberOfBytesRead != 1000)
		{
			break;
		}

		lpDosHeader = (PIMAGE_DOS_HEADER)Buffer;
		if (IsBadReadPtr(lpDosHeader, sizeof(DWORD)))
		{
			break;
		}

		if (lpDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
		{
			break;
		}

		lpNtHeader = (PIMAGE_NT_HEADERS)((PBYTE)lpDosHeader + lpDosHeader->e_lfanew);
		if (IsBadReadPtr(lpNtHeader, sizeof(DWORD)))
		{
			break;
		}

		if (lpNtHeader->Signature != IMAGE_NT_SIGNATURE)
		{
			break;
		}

		bIsPe64 = IsPE64(lpNtHeader);
		
		bIs32BitProcess = Is32BitProcess(dwTargetProcessId);

		if (bIsPe64)
		{
			if (!bIs32BitProcess)
			{
				bRet = TRUE;
			}
		}
		else
		{
			if (bIs32BitProcess)
			{
				bRet = TRUE;
			}
		}
		
	} while (FALSE);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
	}
	
	return bRet;
}
	
