#include "stdafx.h"
#include "CSystemInfo.h"

#include <shlobj.h>
#include <shlwapi.h>
#pragma comment(lib,"shlwapi.lib")
/*!
* @brief CreateLink
*
* 在桌面上创建LNK快捷方式
* @param  iconExePath  指向的目标程序目录
* @param  iconExeFile  指向的目标程序EXE名称
* @param  commline  额外启动参数
* @param  iconName  图标名称
* @param  iconImageFile  图标ICO路径
* @return
*/

void CSystemInfo::CreateLink(const char * iconExePath, const char * iconExeFile, const char * commline, const char * iconName, const char * iconImageFile)
{
	//初始化com组件
	CoInitialize(NULL);

	//获取桌面路径
	CHAR szPath[MAX_PATH] = { 0 };
	SHGetSpecialFolderPathA(0, szPath, CSIDL_DESKTOPDIRECTORY, 0);
	//strcat(szPath,"\\");
	//strcat(szPath,iconName);
	//strcat(szPath,".lnk");
	sprintf_s(szPath, MAX_PATH, "%s\\%s.lnk", szPath, iconName);

	IShellLinkA *pShellLink;
	IPersistFile *pPerFile;

	//创建对象
	CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,
		IID_IShellLink, (LPVOID*)&pShellLink);

	CHAR szSrcPath[MAX_PATH] = "";

	//  从IShellLink获取其IPersistFile接口
	pShellLink->QueryInterface(IID_IPersistFile, (LPVOID*)&pPerFile);

	/*printf("启动路径:%s\n",szSrcPath);
	printf("设置图标:%s\n",iconImageFile);
	printf("保存路径:%s\n",szPath);*/
	WCHAR wszPath[MAX_PATH] = { 0 };
	MultiByteToWideChar(CP_ACP, 0, szPath, -1, wszPath, MAX_PATH);

	//设置启动路径
	pShellLink->SetPath(iconExeFile);
	pShellLink->SetDescription(iconName);
	pShellLink->SetWorkingDirectory(iconExePath);

	//设置图标
	pShellLink->SetIconLocation(iconImageFile, 0);
	pShellLink->SetArguments(commline);
	//保存路径
	pPerFile->Save(wszPath, false);

	//释放！
	pPerFile->Release();
	pShellLink->Release();

	CoUninitialize();
}

/*!
* @brief GetDefaultBrowserPath
*
* 获取系统默认浏览器路径
* @return std::string 默认浏览器路径
*/
std::string CSystemInfo::GetDefaultBrowserPath()
{
	std::string strIEPath;
	HKEY hKey = NULL;
	DWORD dwType = REG_SZ;//定义数据类型  
	long lErr = RegOpenKeyExA(HKEY_CLASSES_ROOT, "http\\shell\\open\\command",
		0, KEY_READ | KEY_WOW64_64KEY, &hKey);
	if (lErr != ERROR_SUCCESS)
		return std::string();
	char szIEPath[MAX_PATH];
	DWORD dwLen = sizeof(szIEPath);
	lErr = RegQueryValueExA(hKey, NULL, 0, &dwType, (LPBYTE)szIEPath, &dwLen);
	if (lErr != ERROR_SUCCESS)
		return std::string();
	RegCloseKey(hKey);
	strIEPath = szIEPath;
	int one = strIEPath.find("\"");
	if (one == std::string::npos)
		return std::string();

	int two = strIEPath.find("\"", 1);
	if (two == std::string::npos)
		return std::string();

	strIEPath = strIEPath.substr(one + 1, two - one - 1);

	if (PathFileExistsA(strIEPath.c_str()) == FALSE)
		return std::string();
	return strIEPath;
}

/*!
* @brief GetIePath
*
* 获取系统IE浏览器路径
* @return std::string IE浏览器路径
*/
std::string CSystemInfo::GetIePath()
{
	std::string strIEPath;
	HKEY hKey = NULL;
	DWORD dwType = REG_SZ;//定义数据类型  
	long lErr = RegOpenKeyExA(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\IEXPLORE.EXE",
		0, KEY_READ | KEY_WOW64_64KEY, &hKey);
	if (lErr != ERROR_SUCCESS)
		return std::string();
	char szIEPath[MAX_PATH];
	DWORD dwLen = sizeof(szIEPath);
	lErr = RegQueryValueExA(hKey, NULL, 0, &dwType, (LPBYTE)szIEPath, &dwLen);
	RegCloseKey(hKey);
	if (lErr != ERROR_SUCCESS)
		return std::string();
	strIEPath = szIEPath;
	if (PathFileExistsA(strIEPath.c_str()) == FALSE)
		return std::string();
	return strIEPath;
}

#pragma comment(lib,"Version.lib")
/*!
* @brief GetIeVer
*
* 获取系统IE主版本号
* @return std::string IE主版本号
*/
std::string CSystemInfo::GetIeVer()
{
	const TCHAR szFilename[] = _T("mshtml.dll");
	DWORD dwMajorVersion = 0, dwMinorVersion = 0;
	DWORD dwBuildNumber = 0, dwRevisionNumber = 0;
	DWORD dwHandle = 0; TCHAR szBuf[80];
	DWORD dwVerInfoSize = GetFileVersionInfoSize(szFilename, &dwHandle);//判断容纳文件版本信息需要一个多大的缓冲区
	if (dwVerInfoSize)
	{
		LPVOID lpBuffer = LocalAlloc(LPTR, dwVerInfoSize);//从堆中分配指定大小的字节数
		if (lpBuffer)
		{
			//从支持版本标记的一个模块里获取文件版本信息
			if (GetFileVersionInfo(szFilename, dwHandle, dwVerInfoSize, lpBuffer))
			{
				VS_FIXEDFILEINFO * lpFixedFileInfo = NULL;
				UINT nFixedFileInfoSize = 0;
				if (VerQueryValue(lpBuffer, TEXT("\\"), (LPVOID*)&lpFixedFileInfo, &nFixedFileInfoSize) && (nFixedFileInfoSize))
				{//从版本资源中获取信息
					dwMajorVersion = HIWORD(lpFixedFileInfo->dwFileVersionMS);//主版本号
					dwMinorVersion = LOWORD(lpFixedFileInfo->dwFileVersionMS);//福版本号
					dwBuildNumber = HIWORD(lpFixedFileInfo->dwFileVersionLS);//编译版本号
					dwRevisionNumber = LOWORD(lpFixedFileInfo->dwFileVersionLS);//修订版本号
				}
			}
			LocalFree(lpBuffer);
		}
	}
	else return 0;
	wchar_t buf[1024] = { 0 };

	swprintf_s(buf, L"IE 版本为 %d.%d.%d.%d", dwMajorVersion, dwMinorVersion, dwBuildNumber, dwRevisionNumber);
	OutputDebugStringW(buf);
	return std::to_string(dwMajorVersion);//返回主版本号
}

#include <winternl.h>
typedef NTSTATUS(NTAPI *PFN_RtlGetVersion)(PRTL_OSVERSIONINFOW lpVersionInformation);
static PFN_RtlGetVersion g_pfnRtlGetVersion = NULL;
/*!
* @brief GetSysVer
*
* 获取系统IE获取系统主版本
* @return DWORD 主版本号
*/
DWORD CSystemInfo::GetSysVer()
{
	// 优化一下
	static BOOL bInited = FALSE;
	static WINDOWS_VERSIONEX verRet = WINDOWS_VERSIONEX::WIN_UNKNOWN;
	if (bInited)
	{
		return verRet;
	}

	RTL_OSVERSIONINFOEXW osex = {};
	osex.dwOSVersionInfoSize = sizeof(osex);
	if (NULL == g_pfnRtlGetVersion)
	{
		HMODULE hNtDll = ::GetModuleHandle(__TEXT("ntdll.dll"));
		g_pfnRtlGetVersion = (PFN_RtlGetVersion)::GetProcAddress(hNtDll, "RtlGetVersion");
		if (NULL != g_pfnRtlGetVersion)
		{
			if (!NT_SUCCESS(g_pfnRtlGetVersion((RTL_OSVERSIONINFOW*)&osex)))
			{
				return verRet;
			}
		}
	}

	WINDOWS_VERSIONEX ver = WINDOWS_VERSIONEX::WIN_UNKNOWN;
	SYSTEM_INFO sysInfo = { 0 };
	::GetSystemInfo(&sysInfo);

	switch (osex.dwMajorVersion)
	{
	case 5:
	{
		if (0 == osex.dwMinorVersion)
		{
			ver = WINDOWS_VERSIONEX::WIN_2000;
		}
		else if (1 == osex.dwMinorVersion)
		{
			ver = WINDOWS_VERSIONEX::WIN_XP;
		}
		else if (2 == osex.dwMinorVersion)
		{
			if ((VER_NT_WORKSTATION == osex.wProductType)
				&& (PROCESSOR_ARCHITECTURE_AMD64 == sysInfo.wProcessorArchitecture))
			{
				ver = WINDOWS_VERSIONEX::WIN_XP_X64;
			}

			if (0 == ::GetSystemMetrics(SM_SERVERR2))
			{
				ver = WINDOWS_VERSIONEX::WIN_SERVER_2003;
			}
			else
			{
				ver = WINDOWS_VERSIONEX::WIN_SERVER_2003_R2;
			}

			if (osex.wSuiteMask & VER_SUITE_WH_SERVER)
			{
				ver = WINDOWS_VERSIONEX::WIN_HOME_SERVER;
			}
		}
	}
	break;

	case 6:
	{
		if (0 == osex.dwMinorVersion)
		{
			if (VER_NT_WORKSTATION == osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_VISTA;
			}
			if (VER_NT_WORKSTATION != osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_SERVER_2008;
			}
		}
		else if (1 == osex.dwMinorVersion)
		{
			if (VER_NT_WORKSTATION != osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_SERVER_2008_R2;
			}
			if (VER_NT_WORKSTATION == osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_7;
			}
		}
		else if (2 == osex.dwMinorVersion)
		{
			if (VER_NT_WORKSTATION != osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_SERVER_2012;
			}

			if (VER_NT_WORKSTATION == osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_8;
			}
		}
		else if (3 == osex.dwMinorVersion)
		{
			if (VER_NT_WORKSTATION != osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_SERVER_2012_R2;
			}

			if (VER_NT_WORKSTATION == osex.wProductType)
			{
				ver = WINDOWS_VERSIONEX::WIN_8_1;
			}
		}
	}
	break;
	case 10:
	{
		if (0 == osex.dwMinorVersion)
		{
			if (VER_NT_WORKSTATION == osex.wProductType)
				ver = WINDOWS_VERSIONEX::WIN_10;
			else
				ver = WINDOWS_VERSIONEX::WIN_SERVER_2016_TP;
		}
	}
	default:
		break;
	}

	// 保存结果
	verRet = ver;
	bInited = TRUE;
	return ver;
}

/*!
* @brief GetSysVer
*
* @param  flg  编号
* 根据编号获取系统主版本字符串
* @return std::string 系统版本
*/
std::string CSystemInfo::GetSysVer(WINDOWS_VERSIONEX flg)
{

	switch (flg)
	{
	case CSystemInfo::WIN_UNKNOWN:
		return std::string("unknown");
		break;
	case CSystemInfo::WIN_2000:
		return std::string("win2000");
		break;
	case CSystemInfo::WIN_XP:
		return std::string("winXp");
		break;
	case CSystemInfo::WIN_XP_X64:
		return std::string("winXp64");
		break;
	case CSystemInfo::WIN_SERVER_2003:
		return std::string("win_server_2003");
		break;
	case CSystemInfo::WIN_HOME_SERVER:
		return std::string("win_home_server");
		break;
	case CSystemInfo::WIN_SERVER_2003_R2:
		return std::string("win_server_2003_r2");
		break;
	case CSystemInfo::WIN_VISTA:
		return std::string("winvista");
		break;
	case CSystemInfo::WIN_SERVER_2008:
		return std::string("win_server_2008");
		break;
	case CSystemInfo::WIN_SERVER_2008_R2:
		return std::string("win_server_2008_r2");
		break;
	case CSystemInfo::WIN_7:
		return std::string("win7");
		break;
	case CSystemInfo::WIN_SERVER_2012:
		return std::string("win_server_2012");
		break;
	case CSystemInfo::WIN_8:
		return std::string("win8");
		break;
	case CSystemInfo::WIN_SERVER_2012_R2:
		return std::string("win_server_2012_r2");
		break;
	case CSystemInfo::WIN_8_1:
		return std::string("win8.1");
		break;
	case CSystemInfo::WIN_10:
		return std::string("win10");
		break;
	case CSystemInfo::WIN_SERVER_2016_TP:
		return std::string("win_server_2016_tp");
		break;
	default:
		return std::string("unknown");
		break;
	}
}

/*!
* @brief CreateLinkIcon 创建桌面图标
*
* @param  目标进程所在目录
* @param  目标进程完整路径
* @param  快捷方式所需参数
* @param  快捷方式名称
* @param  快捷方式所需ICO图标路径
* @return std::string 快捷方式完整路径
*/
std::string CSystemInfo::CreateLinkIcon(const char * iconExePath, const char * iconExeFile, const char * commline, const char * iconName, const char * iconImageFile)
{
	//初始化com组件
	CoInitialize(NULL);

	//获取桌面路径
	CHAR szPath[MAX_PATH] = { 0 };
	//CSIDL_DESKTOPDIRECTORY

	//此处可以设置需要保存的LINK目录。
	SHGetSpecialFolderPathA(0, szPath, CSIDL_DESKTOP, 0);
	//strcat(szPath,"\\");
	//strcat(szPath,iconName);
	//strcat(szPath,".lnk");
	//std::string str = GetTempFilePath(std::string(iconName) + ".lnk");
	//sprintf_s(szPath, MAX_PATH, "%s", str.c_str());

	IShellLinkA *pShellLink;
	IPersistFile *pPerFile;

	//创建对象
	CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,
		IID_IShellLinkA, (LPVOID*)&pShellLink);

	CHAR szSrcPath[MAX_PATH] = "";

	//  从IShellLink获取其IPersistFile接口
	pShellLink->QueryInterface(IID_IPersistFile, (LPVOID*)&pPerFile);

	/*printf("启动路径:%s\n",szSrcPath);
	printf("设置图标:%s\n",iconImageFile);
	printf("保存路径:%s\n",szPath);*/
	WCHAR wszPath[MAX_PATH] = { 0 };
	MultiByteToWideChar(CP_ACP, 0, szPath, -1, wszPath, MAX_PATH);

	//设置启动路径
	pShellLink->SetPath(iconExeFile);
	pShellLink->SetDescription(iconName);
	pShellLink->SetWorkingDirectory(iconExePath);

	//设置图标
	pShellLink->SetIconLocation(iconImageFile, 0);
	if (strlen(commline) > 0)
	{
		pShellLink->SetArguments(commline);
	}

	//保存路径
	pPerFile->Save(wszPath, false);

	//释放！
	pPerFile->Release();
	pShellLink->Release();

	CoUninitialize();
	return szPath;
}

/*!
* @brief CreateTaskBarLinkIcon 根据不同系统创建任务栏快启图标
*
* @param  sysVer	系统版本 WIN7 10,WIN10 15
* @param  linkPath  快捷方式原来目录，也可以是EXE目录,此参数如果提供了完整路径，下面参数可省略
* @param  linkexe   快捷方式完整路径，可以是EXE完整路径
* @return bool
*/
bool CSystemInfo::CreateTaskBarLinkIcon(DWORD sysVer, std::string linkPath, std::string linkexe)
{
	if (linkPath.length() == 0)
	{
		return false;
	}
	char path[MAX_PATH * 4] = "";
	char exeName[MAX_PATH] = "";

	if (linkexe.length() == 0)
	{
		//需使用LINKPATH中切出一个EXE路径来
		sprintf_s(exeName, MAX_PATH, "%s", linkPath.c_str());
		PathStripPathA(exeName);
		sprintf_s(path, MAX_PATH * 4, "%s", linkPath.c_str());
		PathRemoveFileSpecA(path);

	}
	else
	{
		sprintf_s(path, MAX_PATH * 4, "%s", linkPath.c_str());
		PathRemoveFileSpecA(path);
		sprintf_s(exeName, MAX_PATH, "%s", linkexe.c_str());
	}
	if (sysVer == 10 /*WINDOWS_VERSIONEX::WIN_7*/)
	{
		TaskbarPin7((path), exeName, true);
	}
	if (sysVer == 15 /*WINDOWS_VERSIONEX::WIN_10*/)
	{
		//检查是否已经存在同名快启，如果存在则要操作两次，一次删除一次附加新的
		char tepath[1000] = "";
		SHGetSpecialFolderPathA(0, tepath, CSIDL_APPDATA, false);
		std::string strpath = tepath + std::string("\\Microsoft\\Internet Explorer\\Quick Launch\\User Pinned\\TaskBar\\" + std::string(exeName));
		if (!PathFileExistsA(strpath.c_str()))
		{
			//不存在此图标，创建 
			TaskbarPinWin10(path, exeName);
		}


	}

	return true;
}


#include <atlbase.h>
/*!
* @brief TaskbarPinWin10 WIN10 绑定快捷方式到任务栏，已存在则解绑
*
* @param  lpszFilePath  快捷方式原来目录，也可以是EXE目录,此参数如果提供了完整路径，下面参数可省略
* @param  lpszFileName   快捷方式完整路径，可以是EXE完整路径
* @return void
*/
void CSystemInfo::TaskbarPinWin10(LPCSTR lpszFilePath, LPCSTR lpszFileName)
{
	CoInitialize(nullptr);
	do
	{
		TCHAR szVal[MAX_PATH] = { 0 };
		ULONG uValSize = MAX_PATH;
		CRegKey regKey;

		if (ERROR_SUCCESS != regKey.Open(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\CommandStore\\shell\\Windows.taskbarpin"), KEY_READ))
		{
			break;
		}
		if (ERROR_SUCCESS != regKey.QueryStringValue(_T("ExplorerCommandHandler"), szVal, &uValSize))
		{
			break;
		}
		regKey.Close();
		if (ERROR_SUCCESS != regKey.Create(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\*\\shell\\{:}")))
		{
			break;
		}
		if (ERROR_SUCCESS != regKey.SetStringValue(_T("ExplorerCommandHandler"), szVal))
		{
			break;
		}
		regKey.Close();

		IShellDispatch* pShellDisp = NULL;
		Folder *pFolder;
		FolderItem *pFolderItem;
		CComBSTR stitle, str;
		HRESULT hr = S_OK;

		hr = ::CoCreateInstance(CLSID_Shell, NULL,
			CLSCTX_SERVER, IID_IShellDispatch, (LPVOID*)&pShellDisp);
		if (SUCCEEDED(hr))
		{
			hr = pShellDisp->NameSpace(CComVariant(lpszFilePath), &pFolder);
			hr = pFolder->ParseName(CComBSTR(lpszFileName), &pFolderItem);
			if (SUCCEEDED(hr))
			{
				pFolderItem->InvokeVerb(CComVariant(_T("{:}")));
			}
			hr = pShellDisp->Release();
			pShellDisp = NULL;
		}

		if (ERROR_SUCCESS != regKey.Open(HKEY_CURRENT_USER, _T("SOFTWARE\\Classes\\*\\shell"), KEY_READ | KEY_WRITE))
		{
			break;
		}
		regKey.RecurseDeleteKey(_T("{:}"));
		regKey.Close();

	} while (FALSE);
	CoUninitialize();
}

/*!
* @brief TaskbarPin7 WIN7下附加到任务栏上
*
* @param  lpPath	   快捷方式原来目录，也可以是EXE目录,此参数如果提供了完整路径，下面参数可省略
* @param  lpFileName   快捷方式完整路径，可以是EXE完整路径
* @param  bIsPin	   绑或解绑
* @return BOOL
*/
BOOL CSystemInfo::TaskbarPin7(LPSTR lpPath, LPSTR lpFileName, BOOL bIsPin)
{
	WCHAR wszPath[MAX_PATH] = { 0 };
	MultiByteToWideChar(CP_ACP, 0, lpPath, -1, wszPath, MAX_PATH);
	WCHAR wszFileName[MAX_PATH] = { 0 };
	MultiByteToWideChar(CP_ACP, 0, lpFileName, -1, wszFileName, MAX_PATH);

	CoInitialize(nullptr);
	BOOL bRet = FALSE;
	HMENU hmenu = NULL;
	LPSHELLFOLDER pdf = NULL;
	LPSHELLFOLDER psf = NULL;
	LPITEMIDLIST pidl = NULL;
	LPITEMIDLIST pitm = NULL;
	LPCONTEXTMENU pcm = NULL;

	if (SUCCEEDED(SHGetDesktopFolder(&pdf))
		&& SUCCEEDED(pdf->ParseDisplayName(NULL, NULL, wszPath, NULL, &pidl, NULL))
		&& SUCCEEDED(pdf->BindToObject(pidl, NULL, IID_IShellFolder, (void **)&psf))
		&& SUCCEEDED(psf->ParseDisplayName(NULL, NULL, wszFileName, NULL, &pitm, NULL))
		&& SUCCEEDED(psf->GetUIObjectOf(NULL, 1, (LPCITEMIDLIST *)&pitm, IID_IContextMenu, NULL, (void **)&pcm))
		&& (hmenu = CreatePopupMenu()) != NULL
		&& SUCCEEDED(pcm->QueryContextMenu(hmenu, 0, 1, INT_MAX, CMF_NORMAL)))
	{
		CMINVOKECOMMANDINFO ici = { sizeof(CMINVOKECOMMANDINFO), 0 };
		ici.hwnd = NULL;
		ici.lpVerb = bIsPin ? "taskbarpin" : "taskbarunpin";
		pcm->InvokeCommand(&ici);
		bRet = TRUE;
	}

	if (hmenu)
		DestroyMenu(hmenu);
	if (pcm)
		pcm->Release();
	if (pitm)
		CoTaskMemFree(pitm);
	if (psf)
		psf->Release();
	if (pidl)
		CoTaskMemFree(pidl);
	if (pdf)
		pdf->Release();
	CoUninitialize();
	return bRet;
}
