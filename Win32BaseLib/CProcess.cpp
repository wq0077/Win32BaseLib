#include "stdafx.h"
#include "CProcess.h"
namespace Win32BaseLib
{
	/*!
	* @brief CProcess
	*
	* 构造函数,直接使用当前进程
	* @return void
	*/
	CProcess::CProcess()
	{
		SetPid(GetCurrentProcessId());
	}
	/*!
	* @brief CProcess
	*
	* 构造函数
	* @param  pid  进程ID
	* @return void
	*/
	CProcess::CProcess(DWORD pid)
	{
		SetPid(pid);
	}
	/*!
	* @brief SetPid
	*
	* 设置私有属性
	* @param  pid  进程ID
	* @return void
	*/
	void CProcess::SetPid(DWORD pid)
	{
		m_pid = pid;
		if (m_pHandle)
			CloseHandle(m_pHandle);
		m_pHandle =  OpenProcess(PROCESS_ALL_ACCESS,NULL,m_pid);
		m_exeName = "";
	}

	/*!
	* @brief ~CProcess
	*
	* 析构函数
	* @return void
	*/
	CProcess::~CProcess()
	{
		if (m_pHandle)
		{
			CloseHandle(m_pHandle);
		}
	}

	/*!
	* @brief GetCurrentPid
	*
	* 获取指定进程的EXE名称
	* @param  pid  进程ID
	* @return string  进程名
	*/
	std::string CProcess::GetExeName(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, NULL, pid);
		if (handle)
		{
			char baseName[MAX_PATH] = "";
			if (GetModuleBaseNameA(handle, NULL, baseName, MAX_PATH))
			{
				CloseHandle(handle);
				return std::string(baseName);
			}

		}
		return std::string();
	}

	/*!
	* @brief GetCurrentPid
	*
	* 获取当前进程的PID
	* @return DWORD  pid
	*/
	DWORD CProcess::GetCurrentPid()
	{
		return 0;
	}


	std::string CProcess::GetProcessFullPath(DWORD pid)
	{
		HANDLE hpro = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
		if (!hpro)
			return std::string();
		DWORD len = MAX_PATH;
		BOOL bok = false;
		DWORD dw = 0;
		CHAR* path = NULL;
		do
		{
			path = new CHAR[len]();
			bok = QueryFullProcessImageNameA(hpro, 0, path, &len);
			if (bok)
			{
				break;
			}
			else
			{
				dw = GetLastError();
				if (dw == 122)
				{
					delete path;
					path = NULL;
				}
				else
					break;
			}
			len = len * 2;
		} while (true);
		CloseHandle(hpro);
		return std::string(path);
	}

	/*!
	* @brief GetProcessDir
	*
	* 获取指定PID进程EXE所在目录
	* @param  pid     进程PID
	* @return string  进程EXE所在目录
	*/
	std::string CProcess::GetProcessDir(DWORD pid)
	{
		std::string exeDir = GetProcessFullPath(pid);
		char *tempPath = new char[exeDir.length() + 1]();
		sprintf_s(tempPath, exeDir.length() + 1, "%s", exeDir.c_str());
		PathRemoveBlanksA(tempPath);
		PathRemoveFileSpecA(tempPath);
		return tempPath;
	}

	/*!
	* @brief GetProcessCreateTime
	*
	* 运行指定的EXE程序
	* @param  DWORD	pid   进程ID
	* @return DWORD64  时间ULARGE_INTEGER
	*/
	DWORD64 CProcess::GetProcessCreateTime(DWORD pid)
	{
		HANDLE hpro = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
		if (!hpro)
			return 0;
		FILETIME hjCrtime = { 0 };
		FILETIME hjExtime = { 0 };
		FILETIME hjKetime = { 0 };
		FILETIME hjUrtime = { 0 };
		bool bOk = GetProcessTimes(hpro, &hjCrtime, &hjExtime, &hjKetime, &hjUrtime);
		if (hpro)
			CloseHandle(hpro);
		if (!bOk)
			return 0;
		ULARGE_INTEGER cr = { 0 };
		cr.LowPart = hjCrtime.dwLowDateTime;
		cr.HighPart = hjCrtime.dwHighDateTime;
		return (DWORD64)cr.QuadPart;
	}

	/*!
	* @brief CreateProcessSimple
	*
	* 运行指定的EXE程序
	* @param  exePath			string   进程路径
	* @param  parm				string   进程参数
	* @param  wait_exit			bool     等待运行
	* @param  show_window_flag	int		 窗口显示状态
	* @param  dir				string	 工作目录
	* @param  wait_timeout		DWORD	 超时时间		
	* @return bool  成功失败
	*/
	bool CProcess::CreateProcessSimple(std::string exePath, std::string parm, bool wait_exit, int show_window_flag, const std::string & dir, DWORD wait_timeout)
	{
		STARTUPINFOA si = { sizeof(si), 0 };
		PROCESS_INFORMATION pi = { 0 };
		CHAR szCommandLine[1024] = { 0 };
		sprintf_s(szCommandLine, _countof(szCommandLine), "\"%s\" %s", exePath.c_str(), parm.c_str());
		//_snprintf_s(szCommandLine, _countof(szCommandLine), _countof(szCommandLine) - 1, __TEXT("\"%s\" %s"), path.c_str(), param.c_str());

		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = show_window_flag;

		LPCSTR dir_path = NULL;
		if (dir.length() > 0)
		{
			dir_path = dir.c_str();
		}
		if (::CreateProcessA(NULL, szCommandLine, NULL, NULL, FALSE, 0, NULL, dir_path, &si, &pi))
		{
			if (wait_exit)
				WaitForSingleObject(pi.hProcess, wait_timeout);

			if (pi.hProcess)
				::CloseHandle(pi.hThread);
			if (pi.hThread)
				::CloseHandle(pi.hProcess);

			return true;
		}
		else
		{
			return ShellExecuteSimple(exePath, parm, wait_exit, show_window_flag);
		}
	}

	/*!
	* @brief ShellExecuteSimple
	*
	* 运行指定的EXE程序,使用shell
	* @param  exePath			string   进程路径
	* @param  parm				string   进程参数
	* @param  wait_exit			bool     等待运行
	* @param  show_window_flag	int		 窗口显示状态
	* @return bool  成功失败
	*/
	bool CProcess::ShellExecuteSimple(const std::string & path, const std::string & param, bool wait_exit, int show_window_flag)
	{
		SHELLEXECUTEINFOA ShExecInfo = { sizeof(ShExecInfo) };
		ShExecInfo.fMask = SEE_MASK_FLAG_NO_UI;
		if (wait_exit)
			ShExecInfo.fMask |= SEE_MASK_NOCLOSEPROCESS;
		ShExecInfo.lpFile = path.c_str();
		ShExecInfo.lpParameters = param.c_str();
		ShExecInfo.nShow = show_window_flag;
		BOOL bResult = ::ShellExecuteExA(&ShExecInfo);

		if (bResult && wait_exit)
		{
			WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
			CloseHandle(ShExecInfo.hProcess);
		}

		return (TRUE == bResult);
	}


	typedef HMODULE(WINAPI *PFN_LoadLibraryA)(IN LPCSTR lpFileName);
	static PFN_LoadLibraryA g_pfnLoadLibraryA = NULL;
	/*!
	* @brief Inject
	*
	* 远程线程注入目录进程
	* @param  dwProcessId			DWORD   进程PID
	* @param  lpDllFileName			LPCSTR  DLL路径
	* @return bool  成功失败
	*/
	BOOL CProcess::Inject(IN DWORD dwProcessId, IN LPCSTR lpDllFileName)
	{
		if (0 == dwProcessId || NULL == lpDllFileName)
			return FALSE;

		if (!::PathFileExistsA(lpDllFileName))
			return FALSE;

		BOOL bRet = FALSE;
		HANDLE hProcess = NULL;
		LPVOID lpDllName = NULL;
		DWORD dwDllFileNameLength = strlen(lpDllFileName) + 1;
		do
		{
			hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
			if (NULL == hProcess)
				break;

			if (NULL == g_pfnLoadLibraryA)
			{
				g_pfnLoadLibraryA = (PFN_LoadLibraryA)::GetProcAddress(::GetModuleHandleA("Kernel32.dll"), "LoadLibraryA");
				if (NULL == g_pfnLoadLibraryA)
					break;
			}

			lpDllName = ::VirtualAllocEx(hProcess, NULL, dwDllFileNameLength, MEM_COMMIT, PAGE_READWRITE);
			if (NULL == lpDllName)
				break;

			SIZE_T dwWrite = 0;
			if (!::WriteProcessMemory(hProcess, lpDllName, lpDllFileName, dwDllFileNameLength, &dwWrite) || dwWrite != dwDllFileNameLength)
				break;

			HANDLE hThread = ::CreateRemoteThread(hProcess,
				NULL,
				0,
				(LPTHREAD_START_ROUTINE)g_pfnLoadLibraryA,
				lpDllName,
				0,
				NULL);

			if (NULL != hThread)
			{
				::WaitForSingleObject(hThread, INFINITE);
				::CloseHandle(hThread);
				bRet = TRUE;
			}
		} while (FALSE);


		if (hProcess)
			::CloseHandle(hProcess);

		return bRet;
	}

	/*!
	* @brief ThisModuleHandle
	*
	* 获取当前代码所在的模块基址
	* @return HMODULE  模块地址
	*/
	HMODULE CProcess::ThisModuleHandle()
	{
		MEMORY_BASIC_INFORMATION info;
		::VirtualQuery((void*)&ThisModuleHandle, &info, sizeof(info));
		static HMODULE sInstance = (HMODULE)info.AllocationBase;
		return sInstance;
	}
}