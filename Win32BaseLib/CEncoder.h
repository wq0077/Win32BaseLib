#pragma once
/*************************************************
*@file  CEncode.h
*@brief ��д������ص����
**************************************************/
#include <Windows.h>
#include <string>
/*************************************************
*@class CEncode CEncode.h
*@brief ���ֱ��룬������ת��
**************************************************/
namespace Win32BaseLib
{
	class CEncoder
	{
	public:
		CEncoder() {}
		~CEncoder() {}
		static std::string					Md5Str(std::string str,bool isUpper = true,bool is16 = false);
		static std::string					Md5File(std::string filePath, bool isUpper = true, bool is16 = false);

		static std::string					Base64Encode(std::string bin_data);
		static std::string					Base64Decode(std::string  base64);

		static std::string							UrlEncode(const std::string &str);
		static std::string							UrlDecode(const std::string &str);

		static std::string							UTF8UrlEncode(const std::string &str);
		static std::string							UTF8UrlDecode(const std::string &str);

		static std::string							UTF8ToANSI(const std::string &str);
		static std::string						ANSIToUTF8(const std::string &str);

		static std::wstring						string2wstring(std::string& str);
		static std::string						wstring2string(std::wstring& wstr);

		std::string						Utf8Code2String(char* szCode);

		static std::string						UTF8StringToAnsiString(const std::string &strUtf8);
		static std::string						AnsiStringToUTF8String(const std::string& strAnsi);

		static void							UTF8CharToUnicodeChar(WCHAR* pUnicodeBuffer, const char *pUTF8Buffer);
		static void							UnicodeCharToUTF8Char(char* pUTF8Buffer, const WCHAR* pUnicodeBuffer);

		static void							AnsiToUnicode(WCHAR* pUnicodeBuffer, int nUnicodeBufferSize, const char *pAnsiBuffer, int nAnsiBufferSize);
		static void							UnicodeToAnsi(char* pAnsiBuffer, int nAnsiBufferSize, WCHAR* pUnicodeBuffer, int nUnicodeBufferSize);

		static char							CharToInt(char ch);
		static char							StrToBin(char *pString);
	private:

		

		

		

		

	};
}
