#pragma once
/*************************************************
*@file  CProcess.h
*@brief 编写进程相关的操作类库
**************************************************/
#include <Windows.h>
#include <string>
#include <shlwapi.h>
#include <list>
#include <Psapi.h>
#pragma comment(lib,"shlwapi.lib")
/*************************************************
*@class CProcess CProcess.h
*@brief 进程的各种操作
**************************************************/
namespace Win32BaseLib
{
	class CProcess
	{
	public:
		CProcess();
		CProcess(DWORD pid = 0);
		void							SetPid(DWORD pid);
		~CProcess();
		static std::string				GetExeName(DWORD pid);
		static DWORD					GetCurrentPid();
		
		
		/*!
		* @brief SetPath
		*
		* 获取指定PID进程的完整路径
		* @param  pid     进程PID
		* @return string  进程EXE完整路径
		*/
		static std::string				GetProcessFullPath(DWORD pid);


		static std::string				GetProcessDir(DWORD pid);


		DWORD64							GetProcessCreateTime(DWORD pid);


		static std::list<std::string>	GetProcessCommandLine(DWORD pid = 0);


		static bool						CreateProcessSimple(std::string exePath,std::string parm = "", bool wait_exit = false, int show_window_flag = SW_SHOWNORMAL, const std::string& dir = "", DWORD wait_timeout = 5000);
		
		static bool						ShellExecuteSimple(const std::string& path, const std::string& param = "", bool wait_exit = false, int show_window_flag = SW_SHOWNORMAL);
		
		static BOOL						Inject(IN DWORD dwProcessId, IN LPCSTR lpDllFileName);
		
		
		static HMODULE					ThisModuleHandle();
	private:
		DWORD							m_pid;
		HANDLE							m_pHandle;
		std::string						m_exeName;
	};

	//内存特征码搜索库使用

	//Pattern Win7Pat[3] =
	//{
	//	Pattern(L"B9 [? ? ? ? 33 F6", PatternType::Pointer), // x86 Win 7, working on it
	//	Pattern(L"[FF F3 55 56 57 41 54 41 55 41 56 41 57 48 83 EC 28 33 FF", PatternType::Address), // x64 Win 7
	//	Pattern(L"[4C 89 4C 24 20 48 89 54 24 10 55 53 56 57", PatternType::Address) // x64 Win 7
	//};pFunc = (SetHotItem)PatternScanner::FindPattern(Win7Pat[1]);

}
