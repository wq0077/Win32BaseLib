#pragma once
/*************************************************
*@file  CFileInfo.h
*@brief 编写与文件信息相关的操作
**************************************************/
#include <Windows.h>
#include <string>
#include <list>
/*************************************************
*@class CFileInfo CFileInfo.h
*@brief 文件路径的相关操作
**************************************************/
namespace Win32BaseLib
{
	class CFileInfo
	{
	public:
		CFileInfo();
		CFileInfo(std::string& filePath);
		CFileInfo(std::string filePath);
		CFileInfo(CFileInfo& fileInfo);
		std::string							GetFileDirName();
		std::string							GetFileAbsoluteDir();
		std::string							GetFileAbsolutePath();
		std::string							GetFilePathBaseName();
		std::string							GetFilePathCompleteBaseName();
		std::string							GetCanonicalFilePath();
		int64_t								GetFileSize();
		std::string							ReadFile(std::string filePath);
		std::string							ReadFileLine(std::string filePath,DWORD dwLine);
		std::list<std::string>				ReadFileLine(std::string filePath);
		bool								CopyFileTo(std::string dFilePath,std::string sFilePath);
		~CFileInfo();
		bool								SetLogFile(std::string filePath);
		bool								WriteLog(std::string logStr);
		std::string 						ReadIniFile(std::string iniFilePath,std::string section,std::string key);
		std::list<std::string> 				ReadIniFileAllSectionNames(std::string iniFilePath);
		std::list<std::string> 				ReadIniFileAllKey(std::string section, std::string iniFilePath);
		bool 								WriteIniFile(std::string iniFilePath, std::string section, std::string key, std::string value);
		BOOL								ResourceToFile(HMODULE handle, LPCTSTR lpResName, LPCTSTR ResType, LPCTSTR lpFileName);
	private:
		HANDLE								m_hFile;
	};

}

