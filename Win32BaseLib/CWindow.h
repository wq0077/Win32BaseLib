#pragma once
/*************************************************
*@file  CWindow.h
*@brief 编写进程相关的操作类库
**************************************************/
#include <Windows.h>
#include <string>
#include <shlwapi.h>
#include <list>
#include <Psapi.h>
#pragma comment(lib,"shlwapi.lib")
/*************************************************
*@class CWindow CWindow.h
*@brief 各种窗口的操作
**************************************************/
//SetWindowState
//0 : 关闭指定窗口
//1 : 激活指定窗口
//2 : 最小化指定窗口, 但不激活
//3 : 最小化指定窗口, 并释放内存, 但同时也会激活窗口.
//4 : 最大化指定窗口, 同时激活窗口.
//5 : 恢复指定窗口, 但不激活
//6 : 隐藏指定窗口
//7 : 显示指定窗口
//8 : 置顶指定窗口
//9 : 取消置顶指定窗口
//10 : 禁止指定窗口
//11 : 取消禁止指定窗口
//12 : 恢复并激活指定窗口
//13 : 强制结束窗口所在进程.

//GetWindowState
//0 : 判断窗口是否存在
//1 : 判断窗口是否处于激活
//2 : 判断窗口是否可见
//3 : 判断窗口是否最小化
//4 : 判断窗口是否最大化
//5 : 判断窗口是否置顶
//6 : 判断窗口是否无响应

//GetWindow
//0 : 获取父窗口
//1 : 获取第一个儿子窗口
//2 : 获取First 窗口
//3 : 获取Last窗口
//4 : 获取下一个窗口
//5 : 获取上一个窗口
//6 : 获取拥有者窗口
//7 : 获取顶层窗口

class CWindow
{
public:
	CWindow();
	~CWindow();
	bool					CloseWindow(HWND hWnd);
	bool					SetWindowState(HWND hWnd,int flag);
	bool					GetWindowState(HWND hWnd,int flag);
	bool					GetWindow(HWND hWnd, int flag);
	bool					SetWindowSize(HWND hWnd,int width, int height);
	bool					SetWindowTitle(HWND hWnd, std::string title);
	bool					SetWindowTransparent(HWND hWnd, int trans);
	bool					MoveWindow(HWND hWnd,int x, int y);
	std::string				GetWindowTitle(HWND hWnd);
	HWND					GetForegroundFocus();
	HWND					GetForegroundWindow();
	HWND					GetMousePointWindow();
	std::list<HWND>			EnumWindow(HWND parent, std::string title, std::string class_name, int filter);
	//1 : 匹配窗口标题, 参数title有效
	//2 : 匹配窗口类名, 参数class_name有效.
	//4 : 只匹配指定父窗口的第一层孩子窗口
	//8 : 匹配所有者窗口为0的窗口, 即顶级窗口
	//16 : 匹配可见的窗口
	std::list<HWND>			EnumWindowByProcessName(std::string processName, std::string title, std::string class_name, int filter);
	std::list<HWND>			EnumWindowByProcessPid(DWORD pid, std::string title, std::string class_name, int filter);
	std::list<HWND>			FindWindowByProcessName(std::string processName, std::string title, std::string class_name);
	std::list<HWND>			FindWindowByProcessId(DWORD pid, std::string title, std::string class_name);
	DWORD					GetWindowProcessId(HWND hWnd);
	HWND					GetWindowByPid(DWORD dwProcessID);
	std::string				GetWindowProcessPath(HWND hWnd);
	bool					GetWindowRect(HWND hWnd,PRECT &re);

};
