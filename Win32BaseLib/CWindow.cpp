#include "stdafx.h"
#include "CWindow.h"

HWND CWindow::GetWindowByPid(DWORD dwProcessID)
{
	//返回Z序顶部的窗口句柄
	HWND hWnd = ::GetTopWindow(0);
	while (hWnd)
	{
		DWORD pid = 0;
		//根据窗口句柄获取进程ID
		DWORD dwTheardId = ::GetWindowThreadProcessId(hWnd, &pid);

		if (dwTheardId != 0)
		{
			RECT re = { 0 };
			::GetWindowRect(hWnd, &re);
			if (pid == dwProcessID && re.right - re.left > 100)
				return hWnd;
		}

		//返回z序中的前一个或后一个窗口的句柄
		hWnd = ::GetNextWindow(hWnd, GW_HWNDNEXT);
	}
	return NULL;
}
