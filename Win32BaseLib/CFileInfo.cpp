#include "stdafx.h"
#include "CFileInfo.h"

namespace Win32BaseLib
{
	CFileInfo::CFileInfo()
	{
	}

	/*!
	* @brief ReadIniFile
	*
	* 从指定的INI文件中读取指定内容
	* @param  string     文件路径
	* @param  string     小节名
	* @param  string     KEY名
	* @return string  value内容
	*/
	std::string CFileInfo::ReadIniFile(std::string iniFilePath, std::string section, std::string key)
	{
		char str[MAX_PATH] = "";
		GetPrivateProfileStringA(section.c_str(),key.c_str(),"", str,MAX_PATH,iniFilePath.c_str());
		return std::string(str);
	}

	/*!
	* @brief ReadIniFileAllSectionNames
	*
	* 从指定的INI文件中读取所有小节名称
	* @param  string     文件路径
	* @return std::list<std::string>  所有节名称
	*/
	std::list<std::string> CFileInfo::ReadIniFileAllSectionNames(std::string iniFilePath)
	{
		char str[32767] = "";
		DWORD dwOutSize = GetPrivateProfileSectionNamesA(str,32767,iniFilePath.c_str());
		char *buf = str;
		std::list<std::string> strList;
		while (strlen(buf) > 0 && (buf - str) < dwOutSize)
		{
			strList.push_back(buf);
			buf += strlen(buf);
			buf++;
		}

		return strList;

	}

	/*!
	* @brief ReadIniFileAllKey
	*
	* 从指定的INI文件中读取指定小节下所有内容
	* @param  string     小节名
	* @param  string     文件路径
	* @return std::list<std::string>  小节下所有内容
	*/
	std::list<std::string> CFileInfo::ReadIniFileAllKey(std::string section, std::string iniFilePath)
	{
		char str[32767] = "";

		DWORD dwOutSize = GetPrivateProfileSectionA(section.c_str(), str, 32767,iniFilePath.c_str());
		char *buf = str;
		std::list<std::string> strList;
		while (strlen(buf) > 0 && (buf - str) < dwOutSize)
		{
			strList.push_back(buf);
			buf += strlen(buf);
			buf++;
		}

		return strList;
	}

	/*!
	* @brief WriteIniFile
	*
	* 往指定的INI文件中写入指定内容,文件不存在时自动创建
	* @param  string     文件路径
	* @param  string     小节名
	* @param  string     key名,为空时删除整个小节
	* @param  string     value,为空时删除key
	* @return bool 成功或失败
	*/
	bool CFileInfo::WriteIniFile(std::string iniFilePath, std::string section, std::string key, std::string value)
	{
		return WritePrivateProfileStringA(section.c_str(), key.c_str(), value.c_str(), iniFilePath.c_str());
	}
	/*!
	* @brief ResourceToFile
	*
	* 往指定的INI文件中写入指定内容,文件不存在时自动创建
	* @param  HMODULE     资源所在的模块基址
	* @param  LPCTSTR     资源名 MAKEINTRESOURCE(IDR_DLL1)
	* @param  LPCTSTR     资源类型
	* @param  LPCTSTR     目标文件完整路径
	* @return bool 成功或失败
	*/
	BOOL CFileInfo::ResourceToFile(HMODULE handle, LPCTSTR lpResName, LPCTSTR ResType, LPCTSTR lpFileName)
	{
		HRSRC hSrc = ::FindResource(handle, lpResName, ResType);
		if (NULL != hSrc)
		{
			HGLOBAL hRes = ::LoadResource(handle, hSrc);
			if (NULL == hRes)
			{
				return false;
			}
			DWORD dwLength = ::SizeofResource(handle, hSrc);
			LPBYTE lpData = (LPBYTE)::LockResource(hRes);
			HANDLE hFile = INVALID_HANDLE_VALUE;
			BOOL bRet = FALSE;
			do
			{
				hFile = ::CreateFile(lpFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
				if (INVALID_HANDLE_VALUE == hFile)
					break;

				if (0 != ::SetFilePointer(hFile, 0, NULL, FILE_BEGIN))
					break;

				if (!::SetEndOfFile(hFile))
					break;

				DWORD dwWrite = 0;
				if (!::WriteFile(hFile, lpData, dwLength, &dwWrite, NULL))
					break;

				bRet = TRUE;
			} while (FALSE);


			if (INVALID_HANDLE_VALUE != hFile)
				::CloseHandle(hFile);
			return bRet;
		}
		return FALSE;
	}

	CFileInfo::~CFileInfo()
	{
	}

	/*!
	* @brief ReadFile
	*
	* 读取指定文本文件内容
	* @param  string     文件路径
	* @return bool 成功或失败
	*/
	std::string CFileInfo::ReadFile(std::string filePath)
	{
		HANDLE sFile = ::CreateFileA(filePath.c_str(), FILE_READ_DATA, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (!sFile)
			return "";
		LARGE_INTEGER  sHsize = { 0 };
		if (!GetFileSizeEx(sFile, &sHsize))
		{
			CloseHandle(sFile);
			return "";
		}
		char* DataBuffer = new char[sHsize.QuadPart]();

		DWORD dwReadLen = 0;
		if (!::ReadFile(sFile, DataBuffer, sHsize.QuadPart, &dwReadLen, NULL) || dwReadLen == 0)
		{
			CloseHandle(sFile);
			return "";
		}
		std::string str = std::string(DataBuffer);
		if (DataBuffer)
		{
			delete[]DataBuffer;
			DataBuffer = NULL;
		}	
		return str;
	}
}

