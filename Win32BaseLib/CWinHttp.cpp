#include "stdafx.h"
#include "CWinHttp.h"
#include <assert.h>


#define HTTP_READBUF_LEN	1024*1024		//1M的接收缓冲区
HANDLE	CWinHttp::m_hMutex; //线程同步互斥
inline void CloseInternetHandle(HINTERNET* hInternet)
{
	if (*hInternet)
	{
		WinHttpCloseHandle(*hInternet);
		*hInternet = NULL;
	}
}


CWinHttp::CWinHttp(void)
	: m_hInternet(NULL)
	, m_hConnect(NULL)
	, m_hRequest(NULL)
	, m_nConnTimeout(5000)
	, m_nSendTimeout(5000)
	, m_nRecvTimeout(5000)
	, m_bHttps(false)
{
	memset(&m_paramsData, 0, sizeof(HttpParamsData));
	Init();
}

CWinHttp::~CWinHttp(void)
{
	Release();
}

bool CWinHttp::Init()
{
	m_hInternet = ::WinHttpOpen(
		L"Microsoft Internet Explorer",
		WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
		WINHTTP_NO_PROXY_NAME,
		WINHTTP_NO_PROXY_BYPASS,
		0);
	if (NULL == m_hInternet)
	{
		m_paramsData.errcode = HttpErrorInit;
		return false;
	}
	::WinHttpSetTimeouts(m_hInternet, 0, m_nConnTimeout, m_nSendTimeout, m_nRecvTimeout);
	return true;
}

void CWinHttp::Release()
{
	CloseInternetHandle(&m_hRequest);
	CloseInternetHandle(&m_hConnect);
	CloseInternetHandle(&m_hInternet);
}

bool CWinHttp::ConnectHttpServer(LPCWSTR lpIP, WORD wPort)
{
	m_hConnect = ::WinHttpConnect(m_hInternet, lpIP, wPort, 0);
	return m_hConnect != NULL;
}

bool CWinHttp::CreateHttpRequest(LPCWSTR lpPage, HttpRequest type, DWORD dwFlag/*=0*/)
{
	wchar_t* pVerb = (type == HttpGet) ? L"GET" : L"POST";
	m_hRequest = ::WinHttpOpenRequest(
		m_hConnect,
		pVerb,
		lpPage,
		NULL,
		WINHTTP_NO_REFERER,
		WINHTTP_DEFAULT_ACCEPT_TYPES,
		dwFlag);
	return m_hRequest != NULL;
}

void CWinHttp::SetTimeOut(int dwConnectTime, int dwSendTime, int dwRecvTime)
{
	m_nConnTimeout = dwConnectTime;
	m_nSendTimeout = dwSendTime;
	m_nRecvTimeout = dwRecvTime;
}

bool CWinHttp::DownloadFile(LPCSTR lpUrl, LPCSTR lpFilePath)
{

	string strRet;
	wstring strUrl = A2U(string(lpUrl));
	wstring strFilePath = A2U(string(lpFilePath));
	Release();
	bool bRet = false;
	DWORD dwBytesToRead = 0, dwFileSize = 0, dwReadSize = 0, dwRecvSize = 0;
	if (!InitConnect(strUrl.c_str(), HttpGet))
		return false;
	if (!QueryContentLength(dwFileSize))
	{
		m_paramsData.errcode = HttpErrorQuery;
		return false;
	}
	wstring strHeaders;
	bool bQuery = QueryRawHeaders(strHeaders);
	if (bQuery && (strHeaders.find(L"404") != wstring::npos))
	{
		m_paramsData.errcode = HttpError404;
		return false;
	}
	HANDLE hFile = CreateFile(strFilePath.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hFile)
	{
		m_paramsData.errcode = HttpErrorCreateFile;
		return false;
	}
	SetFilePointer(hFile, dwFileSize, 0, FILE_BEGIN);
	SetEndOfFile(hFile);
	SetFilePointer(hFile, 0, NULL, FILE_BEGIN);
	if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
	{
		CloseHandle(hFile);
		DeleteFile(strFilePath.c_str());
		return false;
	}
	void* lpBuff = malloc(HTTP_READBUF_LEN);
	while (true)
	{
		if (dwBytesToRead>HTTP_READBUF_LEN)
		{
			free(lpBuff);
			lpBuff = malloc(dwBytesToRead);
		}
		if (!::WinHttpReadData(m_hRequest, lpBuff, dwBytesToRead, &dwReadSize))
			break;
		DWORD dwWriteByte;
		if (!WriteFile(hFile, lpBuff, dwReadSize, &dwWriteByte, NULL) || (dwReadSize != dwWriteByte))
			break;
		dwRecvSize += dwReadSize;
		if (m_paramsData.callback)
			m_paramsData.callback->OnDownloadCallback(m_paramsData.lpparam, DS_Loading, dwFileSize, dwRecvSize);
		if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
			break;
		if (dwBytesToRead <= 0)
		{
			bRet = true;
			break;
		}
	}
	free(lpBuff);
	CloseHandle(hFile);
	if (!bRet)
	{//下载失败，删除文件
		DeleteFile(strFilePath.c_str());
	}
	return bRet;
}

bool CWinHttp::DownloadFile(LPCSTR lpUrl, LPCSTR lpFilePath, int iNum)
{
	//最大支持10个连接
	int iConect = iNum;
	if (iNum > 10)
	{
		iConect = 10;
	}
	//获取文件大小,生成各线程需要下载的部份
	DWORD dwBytesToRead = 0, dwFileSize = 0, dwReadSize = 0, dwRecvSize = 0;
	wstring strUrl = A2U(string(lpUrl));
	if (!InitConnect(strUrl.c_str(), HttpGet))
		return false;
	if (!QueryContentLength(dwFileSize))
	{
		m_paramsData.errcode = HttpErrorQuery;
		return false;
	}
	Release();
	char FilePathDir[_MAX_PATH] = "";
	sprintf_s(FilePathDir, MAX_PATH, "%s", lpFilePath);
	PathRemoveExtensionA(FilePathDir);
	if (dwFileSize > 0)
	{
		DWORD dwTempLen = dwFileSize / iConect;
		DWORD dwSetLen = 0; //已经分配出去的长度
		for (size_t i = 1; i <= iConect; i++)
		{
			PTHREADDOWNLOAD downloadinfo = new THREADDOWNLOAD();
			sprintf_s(downloadinfo->szUrl, MAX_PATH, "%s", lpUrl);
			sprintf_s(downloadinfo->szTempFileName, MAX_PATH, "%s_%d.temp", FilePathDir, i);
			downloadinfo->bIsOk = false;
			downloadinfo->dwId = i;
			downloadinfo->dwStartPos = dwSetLen;
			if (dwFileSize - dwSetLen >  dwTempLen  && dwFileSize - dwSetLen < 2 * dwTempLen)
			{
				//末尾超长的一点,全部放一个里面
				downloadinfo->dwReadFileLen = dwFileSize - dwSetLen;
			}
			else
			{
				downloadinfo->dwReadFileLen = dwTempLen;
			}
			dwSetLen = dwSetLen + downloadinfo->dwReadFileLen;
			m_ThreadDownInfo.push_back(downloadinfo);
		}

		//开始启动下载线程
		for (size_t i = 0; i < iConect; i++)
		{
			CreateThread(0, 0, (LPTHREAD_START_ROUTINE)&(CWinHttp::DownloadThread), m_ThreadDownInfo.at(i), 0, 0);
		}
		CWinHttp::m_hMutex = CreateMutexA(NULL, FALSE, "");
		//设置线程锁,开始校验每个包的完成度
		HANDLE hFile = CreateFileA(lpFilePath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (INVALID_HANDLE_VALUE == hFile)
		{
			m_paramsData.errcode = HttpErrorCreateFile;
			return false;
		}
		//设置文件的大小
		SetFilePointer(hFile, dwFileSize, 0, FILE_BEGIN);
		SetEndOfFile(hFile);
		SetFilePointer(hFile, 0, NULL, FILE_BEGIN);

		do
		{
			DWORD dReturn = WaitForSingleObject(CWinHttp::m_hMutex, INFINITE);
			//检查完成度
			auto iter = m_ThreadDownInfo.begin();   //获取第五个元素的iterator
			for (iter; iter != m_ThreadDownInfo.end(); iter++)
			{
				//检查每个临时文件
				if ((*iter)->bIsOk)
				{
					DWORD isreadlen = 0;
					//打开这个文件 把内容写到主文件中,并删除此文件
					SetFilePointer(hFile, (*iter)->dwStartPos, 0, FILE_BEGIN);
					HANDLE sFile = CreateFileA((*iter)->szTempFileName, FILE_READ_DATA, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
					BYTE* buf = new BYTE[(*iter)->dwReadFileLen]();
					::ReadFile(sFile, buf, (*iter)->dwReadFileLen, &isreadlen, 0);
					if (sFile)
					{
						CloseHandle(sFile);
					}

					::WriteFile(hFile, buf, (*iter)->dwReadFileLen, &isreadlen, 0);
					//删除临时文件
					::DeleteFileA((*iter)->szTempFileName);
					m_ThreadDownInfo.erase(iter);
					break;
				}

			}

			ReleaseMutex(CWinHttp::m_hMutex);
			Sleep(100);  //可以在这里加个检查各线程是否下完了拼装
			if (m_ThreadDownInfo.size() == 0)
			{
				if (hFile)
				{
					CloseHandle(hFile);
				}
				break;
			}
		} while (true);
	}



	return true;
}
bool CWinHttp::DownloadTempFile(LPCSTR lpUrl, LPCSTR lpFilePath, DWORD start, DWORD readlen)
{

	wstring strHeard = L"Accept:*/*\r\n"   \
		"Pragma:no-cache\r\n"  \
		"Cache-Control:no-cache\r\n"\
		"User-Agent:Mozilla/4.04[en](Win95;I;Nav)\r\n"\
		"Range:bytes=" + std::to_wstring(start) + L"-" + std::to_wstring(start + readlen - 1); //从0开始,要长度减1
	string strRet;
	wstring strUrl = A2U(string(lpUrl));
	wstring strFilePath = A2U(string(lpFilePath));
	Release();
	bool bRet = false;
	DWORD dwBytesToRead = 0, dwFileSize = 0, dwReadSize = 0, dwRecvSize = 0;
	if (!InitConnect(strUrl.c_str(), HttpGet, 0, strHeard.c_str()))
		return false;
	if (!QueryContentLength(dwFileSize))
	{
		m_paramsData.errcode = HttpErrorQuery;
		return false;
	}
	wstring strHeaders;
	bool bQuery = QueryRawHeaders(strHeaders);
	if (bQuery && (strHeaders.find(L"404") != wstring::npos))
	{
		m_paramsData.errcode = HttpError404;
		return false;
	}
	HANDLE hFile = CreateFile(strFilePath.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hFile)
	{
		m_paramsData.errcode = HttpErrorCreateFile;
		return false;
	}
	SetFilePointer(hFile, dwFileSize, 0, FILE_BEGIN);
	SetEndOfFile(hFile);
	SetFilePointer(hFile, 0, NULL, FILE_BEGIN);
	if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
	{
		CloseHandle(hFile);
		DeleteFile(strFilePath.c_str());
		return false;
	}
	void* lpBuff = malloc(HTTP_READBUF_LEN);
	while (true)
	{
		if (dwBytesToRead>HTTP_READBUF_LEN)
		{
			free(lpBuff);
			lpBuff = malloc(dwBytesToRead);
		}
		if (!::WinHttpReadData(m_hRequest, lpBuff, dwBytesToRead, &dwReadSize))
			break;
		DWORD dwWriteByte;
		if (!WriteFile(hFile, lpBuff, dwReadSize, &dwWriteByte, NULL) || (dwReadSize != dwWriteByte))
			break;
		dwRecvSize += dwReadSize;
		if (m_paramsData.callback)
			m_paramsData.callback->OnDownloadCallback(m_paramsData.lpparam, DS_Loading, dwFileSize, dwRecvSize);
		if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
			break;
		if (dwBytesToRead <= 0)
		{
			bRet = true;
			break;
		}
	}
	free(lpBuff);
	CloseHandle(hFile);
	if (!bRet)
	{//下载失败，删除文件
		DeleteFile(strFilePath.c_str());
	}
	return bRet;
}

DWORD CWinHttp::DownloadThread(LPVOID lpParameter)
{
	CWinHttp http;
	PTHREADDOWNLOAD  info = (PTHREADDOWNLOAD)lpParameter;
	bool bok = http.DownloadTempFile(info->szUrl, info->szTempFileName, info->dwStartPos, info->dwReadFileLen);
	if (bok)
	{
		DWORD dReturn = WaitForSingleObject(CWinHttp::m_hMutex, INFINITE);
		//检查完成度
		info->bIsOk = true;
		ReleaseMutex(CWinHttp::m_hMutex);

	}
	return 1;
}


bool CWinHttp::DownloadToMem(LPCWSTR lpUrl, OUT void** ppBuffer, OUT int* nSize)
{
	bool bResult = false;
	BYTE* lpFileMem = NULL;
	void* lpBuff = NULL;
	DWORD dwLength = 0, dwBytesToRead = 0, dwReadSize = 0, dwRecvSize = 0;
	try
	{
		if (!InitConnect(lpUrl, HttpGet))
			throw HttpErrorInit;
		if (!QueryContentLength(dwLength))
			throw HttpErrorQuery;
		wstring strHeaders;
		bool bQuery = QueryRawHeaders(strHeaders);
		if (bQuery && (strHeaders.find(L"404") != wstring::npos))
			throw HttpError404;
		if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
			throw HttpErrorQuery;
		lpFileMem = (BYTE*)malloc(dwLength);
		lpBuff = malloc(HTTP_READBUF_LEN);
		while (true)
		{
			if (dwBytesToRead>HTTP_READBUF_LEN)
			{
				free(lpBuff);
				lpBuff = malloc(dwBytesToRead);
			}
			if (!::WinHttpReadData(m_hRequest, lpBuff, dwBytesToRead, &dwReadSize))
				throw HttpErrorDownload;
			memcpy(lpFileMem + dwRecvSize, lpBuff, dwReadSize);
			dwRecvSize += dwReadSize;
			if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
				throw HttpErrorDownload;
			if (dwBytesToRead <= 0)
			{
				bResult = true;
				break;
			}
		}
	}
	catch (HttpInterfaceError error)
	{
		m_paramsData.errcode = error;
	}
	if (lpBuff)
		free(lpBuff);
	if (bResult)
	{
		*ppBuffer = lpFileMem;
		*nSize = dwRecvSize;
	}
	else
		free(lpFileMem);
	return bResult;
}

void CWinHttp::SetDownloadCallback(IHttpCallback* pCallback, void* pParam)
{
	m_paramsData.callback = pCallback;
	m_paramsData.lpparam = pParam;
}

string CWinHttp::Request(LPCSTR lpUrl, HttpRequest type, LPCSTR lpPostData /*= NULL*/, LPCSTR lpHeader/*=NULL*/)
{
	string strRet;
	wstring strUrl = A2U(string(lpUrl));
	if (!InitConnect(strUrl.c_str(), type, lpPostData, (lpHeader == NULL) ? NULL : A2U(string(lpHeader)).c_str()))
		return strRet;
	DWORD dwBytesToRead, dwReadSize;
	void* lpBuff = malloc(HTTP_READBUF_LEN);
	bool bFinish = false;
	while (true)
	{
		if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
			break;
		if (dwBytesToRead <= 0)
		{
			bFinish = true;
			break;
		}
		if (dwBytesToRead>HTTP_READBUF_LEN)
		{
			free(lpBuff);
			lpBuff = malloc(dwBytesToRead);
		}
		if (!::WinHttpReadData(m_hRequest, lpBuff, dwBytesToRead, &dwReadSize))
			break;
		strRet.append((const char*)lpBuff, dwReadSize);
	}
	free(lpBuff);
	if (!bFinish)
		strRet.clear();
	return strRet;
}

string CWinHttp::Request(LPCWSTR lpUrl, HttpRequest type, LPCSTR lpPostData /*= NULL*/, LPCWSTR lpHeader/*=NULL*/)
{
	string strRet;
	if (!InitConnect(lpUrl, type, lpPostData, lpHeader))
		return strRet;
	DWORD dwBytesToRead, dwReadSize;
	void* lpBuff = malloc(HTTP_READBUF_LEN);
	bool bFinish = false;
	while (true)
	{
		if (!::WinHttpQueryDataAvailable(m_hRequest, &dwBytesToRead))
			break;
		if (dwBytesToRead <= 0)
		{
			bFinish = true;
			break;
		}
		if (dwBytesToRead>HTTP_READBUF_LEN)
		{
			free(lpBuff);
			lpBuff = malloc(dwBytesToRead);
		}
		if (!::WinHttpReadData(m_hRequest, lpBuff, dwBytesToRead, &dwReadSize))
			break;
		strRet.append((const char*)lpBuff, dwReadSize);
	}
	free(lpBuff);
	if (!bFinish)
		strRet.clear();
	return strRet;
}

bool CWinHttp::QueryRawHeaders(OUT wstring& strHeaders)
{
	bool bRet = false;
	DWORD dwSize;
	BOOL bResult = ::WinHttpQueryHeaders(m_hRequest, WINHTTP_QUERY_RAW_HEADERS, WINHTTP_HEADER_NAME_BY_INDEX, NULL, &dwSize, WINHTTP_NO_HEADER_INDEX);
	if (ERROR_INSUFFICIENT_BUFFER == GetLastError())
	{
		wchar_t* lpData = (wchar_t*)malloc(dwSize);
		bResult = ::WinHttpQueryHeaders(m_hRequest, WINHTTP_QUERY_RAW_HEADERS, WINHTTP_HEADER_NAME_BY_INDEX, lpData, &dwSize, WINHTTP_NO_HEADER_INDEX);
		if (bResult)
		{
			strHeaders = lpData;
			bRet = true;
		}
		free(lpData);
	}
	return bRet;
}

bool CWinHttp::QueryContentLength(OUT DWORD& dwLength)
{
	bool bRet = false;
	wchar_t szBuffer[30] = { 0 };
	DWORD dwSize = 30 * sizeof(wchar_t);
	if (::WinHttpQueryHeaders(m_hRequest, WINHTTP_QUERY_CONTENT_LENGTH, WINHTTP_HEADER_NAME_BY_INDEX, szBuffer, &dwSize, WINHTTP_NO_HEADER_INDEX))
	{
		TCHAR *p = NULL;
		dwLength = wcstoul(szBuffer, &p, 10);
		bRet = true;
	}
	return bRet;
}

bool CWinHttp::InitConnect(LPCWSTR lpUrl, HttpRequest type, LPCSTR lpPostData/*=NULL*/, LPCWSTR lpHeader/*=NULL*/)
{
	Release();
	if (!Init())
		return false;
	wstring strHostName, strPage;
	WORD wPort;
	MyParseUrlW(lpUrl, strHostName, strPage, wPort);
	if (wPort == INTERNET_DEFAULT_HTTPS_PORT)
		m_bHttps = true;
	if (!ConnectHttpServer(strHostName.c_str(), wPort))
	{
		m_paramsData.errcode = HttpErrorConnect;
		return false;
	}
	DWORD dwFlag = m_bHttps ? WINHTTP_FLAG_SECURE : 0;
	if (!CreateHttpRequest(strPage.c_str(), type, dwFlag))
	{
		m_paramsData.errcode = HttpErrorInit;
		return false;
	}
	if (m_bHttps)
	{
		DWORD dwFlags = SECURITY_FLAG_SECURE |
			SECURITY_FLAG_IGNORE_UNKNOWN_CA |
			SECURITY_FLAG_IGNORE_CERT_WRONG_USAGE |
			SECURITY_FLAG_IGNORE_CERT_CN_INVALID |
			SECURITY_FLAG_IGNORE_CERT_DATE_INVALID;
		WinHttpSetOption(m_hRequest, WINHTTP_OPTION_SECURITY_FLAGS, &dwFlags, sizeof(dwFlags));
	}
	if (!SendHttpRequest(lpPostData, lpHeader))
	{
		m_paramsData.errcode = HttpErrorSend;
		return false;
	}
	if (!WinHttpReceiveResponse(m_hRequest, NULL))
	{
		m_paramsData.errcode = HttpErrorInit;;
		return false;
	}
	return true;
}

bool CWinHttp::SendHttpRequest(LPCSTR lpPostData/*=NULL*/, LPCWSTR lpHeader/*=NULL*/)
{
	DWORD dwSize = (NULL == lpPostData) ? 0 : strlen(lpPostData);
	if (lpHeader == NULL)
		return ::WinHttpSendRequest(m_hRequest, WINHTTP_NO_ADDITIONAL_HEADERS, 0, (LPVOID)lpPostData, dwSize, dwSize, NULL) == TRUE;
	return ::WinHttpSendRequest(m_hRequest, lpHeader, -1L, (LPVOID)lpPostData, dwSize, dwSize, NULL) == TRUE;
}




