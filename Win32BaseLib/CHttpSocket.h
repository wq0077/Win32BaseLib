#pragma once

#include <string>
#include <map>
using std::wstring;
using std::string;
using std::map;

#include <WinSock2.h>
#pragma comment (lib, "ws2_32")
enum HttpRequest
{
	HttpGet = 0,
	HttpPost,
};
//枚举下载状态
enum DownloadState
{
	DS_Loading = 0,
	DS_Fialed,
	DS_Finished,
};

/******************************************************
*定义错误信息
*
******************************************************/
enum HttpInterfaceError
{
	HttpErrorSuccess = 0,		//成功
	HttpErrorInit,				//初始化失败
	HttpErrorConnect,			//连接HTTP服务器失败
	HttpErrorSend,				//发送请求失败
	HttpErrorQuery,				//查询HTTP请求头失败
	HttpError404,				//页面不存在
	HttpErrorIllegalUrl,		//无效的URL
	HttpErrorCreateFile,		//创建文件失败
	HttpErrorDownload,			//下载失败
	HttpErrorQueryIP,			//获取域名对应的地址失败
	HttpErrorSocket,			//套接字错误
	HttpErrorUserCancel,		//用户取消下载
	HttpErrorBuffer,			//文件太大，缓冲区不足
	HttpErrorHeader,			//HTTP请求头错误
	HttpErrorParam,				//参数错误，空指针，空字符……
	HttpErrorWriteFile,			//写入文件失败
	HttpErrorUnknow,

};

//下载的回调
class IHttpCallback
{
public:
	virtual void	OnDownloadCallback(void* pParam, DownloadState state, double nTotalSize, double nLoadSize) = 0;
	virtual bool	IsNeedStop() = 0;
};

///////////////////////////////////////////////////////////////////////////////////////

struct HttpParamsData
{
	void *lpparam;
	IHttpCallback *callback;
	HttpInterfaceError errcode;
};

enum InterfaceType
{
	TypeSocket = 0,
	TypeWinInet,
	TypeWinHttp,
};




//定义HTTP请求头
struct HTTP_HERDER
{
	char szType[5];				//请求类型，GET或者POST
	char szRequest[MAX_PATH];	//请求内容
	char szVersion[9];			//HTTP版本，固定值：HTTP/1.1
	char szAccept[100];			//请求文件类型，默认值为：nAccept: */*
	char szHostName[30];		//服务器域名地址，例如：www.baidu.com
	char szRange[11];			//断点续传起始点，Range: bytes=*-
	char szConnection[11];		//连接方式，默认：Keep-Alive
	HTTP_HERDER()
	{
		memset(this, 0, sizeof(HTTP_HERDER));
		strcpy_s(szVersion, "HTTP/1.1");
		strcpy_s(szConnection, "Keep-Alive");
	}
	string ToString()
	{
		string strRet;
		strRet.append(szType);
		strRet.append(" ");
		strRet.append(szRequest);
		strRet.append(" ");
		strRet.append(szVersion);
		strRet.append("\r\nAccept: ");
		strRet.append(szAccept);
		strRet.append("\r\nHost: ");
		strRet.append(szHostName);
		if (strlen(szRange)>0)//加上断点续传信息
		{
			strRet.append("\r\nRange: bytes=");
			strRet.append(szRange);
			strRet.append("-");
		}
		strRet.append("\r\nConnection: ");
		strRet.append(szConnection);
		strRet.append("\r\n\r\n");
		return strRet;
	}
};

// 每次读取的字节数
#define READ_BUFFER_SIZE		4096
#define DOWNLOAD_BUFFER_SIZE	8*1024*1024	//8M的缓存

class CHttpSocket
{
public:
	CHttpSocket();
	virtual ~CHttpSocket();
	virtual bool	DownloadFile(LPCSTR lpUrl, LPCSTR lpFilePath);
	virtual void	SetDownloadCallback(IHttpCallback* pCallback, void* pParam);
	virtual HttpInterfaceError GetErrorCode() { return m_paramsData.errcode; }
	virtual	LPCWSTR	GetIpAddr()const { return m_strIpAddr.c_str(); }
	virtual	void	FreeInstance() { delete this; }
	virtual bool	DownloadToMem(LPCWSTR lpUrl, OUT void** ppBuffer, OUT int* nSize);

protected:
	class CHttpHeader
	{
	public:
		CHttpHeader(const char* pHeader);
		CHttpHeader(const std::string& strHeader);
		virtual		~CHttpHeader(void);
		//********************************
		//外部接口
		//********************************
		//获取HTTP版本
		const char*		GetHttpVersion()const { return m_szHttpVersion; }
		//获取HTTP服务器返回值
		const int		GetReturnValue()const { return m_uReturnValue; }
		//获取HTTP返回字符
		const char*		GetContent()const { return m_strContent.c_str(); }
		//获取某一个键对应的值
		std::string		GetValue(const std::string& strKey);
	protected:
		//解析HTTP头结构
		bool	Revolse(const std::string& strHeader);
	private:
		//HTTP服务器版本
		char		m_szHttpVersion[9];
		//返回值
		int		m_uReturnValue;
		//返回说明字符串
		std::string	m_strContent;
		//返回的键值对
		std::map<std::string, std::string>	m_ValueMap;
	};

protected:
	bool	InitSocket(const string& strHostName, const WORD sPort);
	void	InitRequestHeader(HTTP_HERDER& header, LPCSTR pRequest, HttpRequest type = HttpGet, LPCSTR pRange = NULL, const char* pAccept = "*/*");

private:
	SOCKET	m_socket;
	wstring	m_strIpAddr;
	HttpParamsData m_paramsData;
};